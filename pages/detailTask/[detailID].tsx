import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import axiosConfig from "@/axiosConfig/axiosConfig";
import { DeadLine } from "@prisma/client";
import { Avatar, Spin } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { LoadingOutlined } from "@ant-design/icons";
import { setloading } from "@/redux/counterSlice";
import { useSession } from "next-auth/react";
type Props = {
  id: string;
};

enum Status {
  TODO = "TODO",
  PROCESSING = "PROCESSING",
  DONE = "DONE",
}

export interface IDetailProps {
  users: {
    assignedAt: string;
    userID: string;
    message:string;
    user: {
      username: string;
      email: string;
      id:string
    };
   
  }[];
  id: string;
  name: string;
  status: Status;
  deadStart?: string;
  deadEnd?: string;
  deadline?: DeadLine;
  priority: number;
  point: number;
  author?: {
    email: string;
    username: string;
    id: string;
  };
  task?: [];
}

const DetailTask = (props: Props) => {
  const { data: session, status } = useSession();
  const dispatch = useDispatch();
  const router = useRouter();
  const [dataDetail, setDataDetail] = useState<IDetailProps>();
  const counter = useSelector(
    (state: {
      counter: {
        loading: boolean;
      };
    }) => state.counter
  );
  const { loading } = counter;
  useEffect(() => {
    dispatch(setloading(true));
    if (router.isReady) {
      axiosConfig
        .get(`/api/task/${router.query.detailID}`)
        .then((value) => {
          if (value.data.dataDetail.length == 0) {
            router.push("/");
          }
          setDataDetail(value.data.dataDetail[0]);
          dispatch(setloading(false));
        })
        .catch((value) => {
          console.log(value);
        });
    }
  }, [router]);
  const dateNow = new Date(Date.now());
  const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;
  let A = dataDetail?.users.find(value=>value.userID == session?.user?.name)

  return (
    <div
      className={`flex relative items-center bg-gray-200 justify-center w-screen h-screen`}
    >
      {loading ? (
        <div className="absolute w-full h-full bg-white top-0 left-0 z-50 opacity-50 flex items-center justify-center ">
          <Spin className="!opacity-100" indicator={antIcon} />
        </div>
      ) : (
        ""
      )}
      <div
        className={`flex flex-col  border    w-10/12 h-5/6 bg-white rounded-md  shadow-2xl	shadow-black-500/50 p-4 ${
          dataDetail?.status == Status.DONE
            ? " shadow-green-500/50 border-green-900"
            : dataDetail?.status == Status.PROCESSING
            ? " shadow-yellow-500/50 border-yellow-900"
            : "shadow-red-500/50 border-red-900"
        }`}
      >
        <h1 className="mb-2">
          <span className="text-3xl">{dataDetail?.name} </span>
          <span className=" bottom-50 left-24 text-sm text-black shadow-xl bg-white w-22 px-3 py-1 rounded  ">
            {new Date(dataDetail?.deadStart ? dataDetail.deadStart : "") <=
              dateNow &&
            new Date(dataDetail?.deadEnd ? dataDetail.deadEnd : "") >=
              dateNow ? (
              <span className="text-yellow-500">Đang diễn ra</span>
            ) : new Date(dataDetail?.deadStart ? dataDetail.deadStart : "") <
              dateNow ? (
              <span className="text-red-500">Đã hết hạn</span>
            ) : !dataDetail?.deadStart && !dataDetail?.deadEnd ? (
              ""
            ) : (
              <span className="text-green-500">Sắp tới</span>
            )}
          </span>
        </h1>
        <div className=" w-full h-full grid grid-cols-2 gap-2">
          <div className=" shadow-lg">
            <h1>Details</h1>
            <div>
              <ul>
                <li className="border flex">
                  <div className="w-24  p-4 border-r">Task:</div>{" "}
                  <div className=" p-4">{dataDetail?.name}</div>{" "}
                </li>
                <li className="border flex">
                  <div className="w-24 p-4 border-r ">Author:</div>{" "}
                  <div className=" p-4">{dataDetail?.author?.email}</div>
                </li>
                <li className="border flex">
                  <div className="w-24  p-4 border-r">Status:</div>{" "}
                  <div className=" p-4">{dataDetail?.status}</div>
                </li>
                <li className="border flex">
                  <div className="w-24  p-4 border-r">DeadLine:</div>{" "}
                  <div className=" p-4">{dataDetail?.deadline}</div>
                </li>
                <li className="border flex">
                  <div className="w-24  p-4 border-r">priority:</div>{" "}
                  <div className=" p-4">
                    {dataDetail?.priority == 0
                      ? "Chưa có"
                      : dataDetail?.priority}
                  </div>
                </li>
                <li className="border flex">
                  <div className="w-24  p-4 border-r">Point:</div>{" "}
                  <div className=" p-4">
                    {dataDetail?.point == 0 ? "Chưa có" : dataDetail?.point}
                  </div>
                </li>
                <li className="border flex">
                  <div className="w-24  p-4 border-r">DateStart:</div>{" "}
                  <div className=" p-4">
                  {new Date(
                      dataDetail?.deadStart ? dataDetail.deadStart : Date.now()
                    ).toLocaleTimeString()} --
                    {new Date(
                      dataDetail?.deadStart ? dataDetail.deadStart : Date.now()
                    ).toLocaleDateString()}
                    
                  </div>
                </li>
                <li className="border flex">
                  <div className="w-24  p-4 border-r">DateEnd:</div>{" "}
                  <div className=" p-4">
                  {new Date(
                      dataDetail?.deadEnd ? dataDetail.deadEnd : Date.now()
                    ).toLocaleTimeString()} --
                    {new Date(
                      dataDetail?.deadEnd ? dataDetail.deadEnd : Date.now()
                    ).toLocaleDateString()}
                   
                  </div>
                </li>
                {session?.user?.name !== dataDetail?.author?.id ? 
                <li className="border flex">
                <div className="w-24  p-4 border-r">Message:</div>{" "}
                <div className=" p-4">
                   {A?.message}
                </div>
              </li> : ''}
              </ul>
            </div>
          </div>
          <div className=" shadow-lg px-4">
            <h1>Assignes {`(${dataDetail?.users.length})`}</h1>
            <div className="">
              <ul>
                {dataDetail?.users.map((value) => {
                  return (
                    <li key={value.userID} className="my-2 flex items-center relative">
                      <Avatar
                        style={{ backgroundColor: "#fde3cf", color: "#f56a00" }}
                        className="mr-2 px-4"
                      >
                        {value.user.username.charAt(0).toLocaleUpperCase()}
                      </Avatar>
                      <div>
                        <div> {value.user.email}</div>
                        <div className="text-xs"> {value.user.username}</div>
                      </div>
                      <p className="absolute top-0 right-0 ">
                        <span className="text-xs">
                          {new Date(
                            value.assignedAt ? value.assignedAt : Date.now()
                          ).toLocaleTimeString()}
                        </span>
                        <span className="text-xs">
                          {new Date(
                            value.assignedAt ? value.assignedAt : Date.now()
                          ).toLocaleDateString()}
                        </span>
                      </p>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailTask;
