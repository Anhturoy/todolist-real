import React, { Dispatch, SetStateAction } from "react";
import { IPageProps } from "../index";
import { setEditToggle } from "@/redux/counterSlice";
import { useDispatch } from "react-redux";


interface RadioOption {
  label: string;
  value: string;
}
enum Status {
  DONE,
  PROCESSING,
  TODO,
}

interface RadioFormProps {
  options: RadioOption[];
  dataEdit: IPageProps | undefined  ,
  setdataEdit: Dispatch<SetStateAction<IPageProps | undefined  >>,
}

const RadioForm: React.FC<RadioFormProps> = ({
  options,
  setdataEdit,
  dataEdit,
}) => {
  let check = (dataEdit?.status)?.toString();
  const dispatch = useDispatch()
  const handleOptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setEditToggle(true))
    let newdata = { ...dataEdit, status:  event.target.value} as IPageProps;
    setdataEdit(newdata)
    ;
  };
  return (
    <form>
      {options?.map((val) => (
        <div key={val.value} className="mt-2 ">
          <label className="flex justify-flex-start items-center">
            <input
              className="mr-1 w-4 h-4"
              type="radio"
              value={val.value}
              checked={check == val.value}
              onChange={handleOptionChange}
            />
            <span
              className={`${
                val.value == Status[0]
                  ? "text-green-500"
                  : val.value == Status[1]
                  ? "text-yellow-500"
                  : "text-red-500"
              }`}
            >
              {val.label}
            </span>
          </label>
        </div>
      ))}
    </form>
  );
};

export default RadioForm;
