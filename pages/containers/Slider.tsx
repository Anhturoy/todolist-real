import React, { Dispatch, SetStateAction } from 'react';
import { Col, InputNumber, Row, Slider } from 'antd';
import { IPageProps } from '..';

interface SliderProps {
    max: number
    keys: string
    setdataEdit: Dispatch<SetStateAction<IPageProps | undefined  >>
    dataEdit: IPageProps | undefined  
  }
 const IntegerStep: React.FC<SliderProps> = ({
    max,
    keys,
    setdataEdit,
    dataEdit
}) => {

  
  const onChange = (newValue: number | null) => {
    let newdata = {...dataEdit, [`${keys}`]: newValue } as IPageProps
    setdataEdit(newdata)
    
  };

  const Newinputvalue = (dataEdit ? dataEdit[keys as keyof IPageProps]  : 0 ) as number

  return (
    <Row>
      <Col span={12}>
        <Slider
          min={0}
          max={max}
          onChange={onChange}
          value={typeof Newinputvalue === 'number' ? Newinputvalue : 0}
        />
      </Col>
      <Col span={4}>
        <InputNumber
          min={0}
          max={max}
          style={{ margin: '0 16px' }}
          value={Newinputvalue}
          onChange={onChange}
        />
      </Col>
    </Row>
  );
};

export default IntegerStep