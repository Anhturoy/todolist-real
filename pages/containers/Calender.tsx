import React, { useEffect, useState } from "react";
import { IPageProps } from "..";
import {
  LoadingOutlined,
  SearchOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/router";
import { Loading } from "@nextui-org/react";
import {
  Avatar,
  Button,
  DatePicker,
  Drawer,
  Dropdown,
  Empty,
  Input,
  MenuProps,
  Modal,
  Select,
  Space,
  Spin,
  Tag,
  message,
} from "antd";
import axiosConfig from "@/axiosConfig/axiosConfig";
import { Calendar } from "antd";
import { Dayjs } from "dayjs";
import { useDispatch, useSelector } from "react-redux";
import { Status } from "@prisma/client";
import { useSession } from "next-auth/react";
import dayjs from "dayjs";
import "react-lazy-load-image-component/src/effects/blur.css";
import type { DatePickerProps, SelectProps } from "antd";
import {
  setAlldataTasks,
  setEditToggle,
  setTasks,
  setTasksBackUp,
  setdataCalender,
  setloading,
} from "@/redux/counterSlice";
import { usePathname } from "next/navigation";
import { AiOutlineRollback } from "react-icons/ai";
import {
  filterOptionDeadline,
  filterOptionPoint,
  filterOptionPriority,
  filterOptionStatus2,
} from "@/option";
import { CalendarMode } from "antd/es/calendar/generateCalendar";
import { IDetailProps } from "../detailTask/[detailID]";
import RadioForm from "../containers/RadioForm";
import utc from "dayjs/plugin/utc";
import IntegerStep from "../containers/Slider";
import { AiOutlineDelete } from "react-icons/ai";

type Props = {
  id: string;
};

enum DeadLine {
  OVER = "OVER",
  RUNNING = "RUNNING",
  COMING = "COMING",
}

export interface DataFilters {
  status: string | null;
  point: string | null;
  priority: string | null;
  deadline: string | null;
  assigned: string | null;
  doeDateStart: string | null;
  doeDateend: string | null;
}

interface ToggleEdits {
  name: boolean;
  status: boolean;
  deadline: boolean;
  point: boolean;
  priority: boolean;
  date: boolean;
}

interface DataAssigne {
  user: {
    id: string;
    username: string;
  };
}
const Calenders = (props: Props) => {
  const router = useRouter();
  const query = router.query;
  const { data: session, status } = useSession();
  const dispatch = useDispatch();
  const [select, setselect] = useState(query);

  const [optionTable, setoptionTable] = useState({
    duration: 0,
    end: "",
    tableEnd: "",
  });

  const counter = useSelector(
    (state: {
      counter: {
        loading: boolean;
        dataEdit: IPageProps;
        dataTasks: IPageProps[];
        dataTasksBackUp: IPageProps[];
        imageUrl: string;
        createLoading: boolean;
        uploadImgLoading: boolean;
        AlldataTasks: IPageProps[];
        dataCalender: IPageProps[];
        imageList: { id: string; filename: string }[];
        editToggle: boolean;
      };
    }) => state.counter
  );
  const { dataCalender, loading, dataTasks, dataTasksBackUp, editToggle } =
    counter;
  const [currentDates, setCurrentDate] = useState<
    string | string[] | undefined
  >();
  const [dataFilter, setdataFilter] = useState<DataFilters>({
    status: null,
    point: null,
    priority: null,
    deadline: null,
    assigned: null,
    doeDateStart: null,
    doeDateend: null,
  });
  useEffect(() => {
    if (router.isReady) {
      dispatch(setEditToggle(false))
      setDoeDate({
        doeDateStart: query.doeDateStart ? query.doeDateStart.toString() : "",
        doeDateEnd: query.doeDateend ? query.doeDateend.toString() : "",
      });
      const currentDate = new Date();
      const currentMonth = query.currentMonth
        ? query.currentMonth
        : currentDate.getMonth();

      dispatch(setloading(true));

      let dataquery = query as unknown as DataFilters;
      let year = dayjs(query.currentDates?.toString()).year();

      setdataFilter(dataquery);
      const startOfMonth = dayjs().startOf("month");
      const endOfMonth = dayjs(`${year}-${Number(currentMonth) + 1}`).endOf(
        "month"
      );

      const firstCellDate = query.firstCellDate
        ? dayjs(query.firstCellDate.toString())
        : startOfMonth.subtract(startOfMonth.day(), "day");

      const duration = dayjs(endOfMonth.format("YYYY-MM-DD")).diff(
        dayjs(firstCellDate.format("YYYY-MM-DD")),
        "day"
      );

      const lastCellDate = query.lastCellDate
        ? dayjs(query.lastCellDate.toString())
        : endOfMonth.add(6 - endOfMonth.day(), "day");

      setoptionTable({
        duration: duration,
        end: endOfMonth.toString(),
        tableEnd: lastCellDate.format("YYYY-MM-DD").toString(),
      });
      setCurrentDate(query.currentDates);
      setselect(query);
      axiosConfig
        .get(`/api/task`, {
          params: {
            ...query,
            currentMonth,
            firstCellDate: !query.firstCellDate
              ? firstCellDate.startOf("day").format("YYYY-MM-DDTHH:mm:ss.SSSZ")
              : query.firstCellDate,
            lastCellDate: !query.lastCellDate
              ? lastCellDate.endOf("day").format("YYYY-MM-DDTHH:mm:ss.SSSZ")
              : query.lastCellDate,
          },
        })
        .then((value) => {
          dispatch(setdataCalender(value.data.datas));
          dispatch(setTasks(value.data.datas));
          dispatch(setTasksBackUp(value.data.datas));
          dispatch(setAlldataTasks(value.data.datas));
          dispatch(setloading(false));
        })
        .catch((value) => {
          console.log(value);
        });
    }
  }, [router]);

  const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;
  const pathname = usePathname();
  const [nameSearch, setNameSearch] = useState(query.name);

  const handleNameSearch = async (event: React.KeyboardEvent<HTMLElement>) => {
    if (event.key == "Enter") {
      router.push({
        query: { ...query, name: nameSearch },
      });
    }
  };

  const handleChangeNameSearch = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setNameSearch(event.target.value);
    if (event.target.value == "") {
      router.push({
        query: { ...query, name: null },
      });
    }
  };
  let data = dataCalender
    ? [...dataCalender].map((value) => {
        value = {
          ...value,
          deadStart: dayjs(value.deadStart).format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
          deadEnd: dayjs(value.deadEnd).format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
        };
        return value;
      })
    : [];

  const [itemHover, setItemHover] = useState("");
  const [dataassigne, setDataAssigne] = useState<DataAssigne[]>();
  const handleMouseEnter = (data: [], id: string) => {
    setDataAssigne(data);
    setItemHover(id);
  };

  const handleMouseLeave = () => {
    setItemHover("");
  };
  const tasks = data;

  const dateCellRender = (value: Dayjs) => {
    const duration = dayjs(optionTable.end).diff(dayjs(value), "day");
    const endDate = dayjs(optionTable.end);

    // Tìm ngày thứ 7 trong tuần đó
    const nearestSaturday = endDate.endOf("week").day(6);
    // Tính số ngày từ ngày ban đầu đến ngày thứ 7 trong tuần đó
    const daysDiff = nearestSaturday.diff(endDate, "day");
    if (typeof window !== "undefined") {
      const tdElements = document.querySelectorAll(
        `td[title="${dayjs(value).format("YYYY-MM-DD")}"]`
      );
      tdElements.forEach((tdElement) => {
        const element = tdElement as HTMLElement;
        element.style.visibility = "visible";
        element.style.background = "none";
      });

      if (dayjs(value) > dayjs(optionTable.end) && optionTable.duration == 35) {
        const tdElements = document.querySelectorAll(
          `td[title="${dayjs(value).format("YYYY-MM-DD")}"]`
        );
        tdElements.forEach((tdElement) => {
          const element = tdElement as HTMLElement;
          element.style.visibility = "hidden";
        });
      }
      if (duration + (daysDiff - 1) < 0 && optionTable.duration < 35) {
        const tdElements = document.querySelectorAll(
          `td[title="${dayjs(value).format("YYYY-MM-DD")}"]`
        );
        tdElements.forEach((tdElement) => {
          const element = tdElement as HTMLElement;
          element.style.visibility = "hidden";
        });
      }
    }

    const dateString = value.format("YYYY-MM-DD");

    if (query.mode == "year") {
      const month = value.month() + 1;
      const tasksForMonth = tasks.filter((task) => {
        const taskMonth = dayjs(task.deadStart).month() + 1;
        return taskMonth === month;
      });
      const taskCountsPerMonth: { [month: number]: number } = {};
      tasks.map((task) => {
        const month = dayjs(task.deadStart).month() + 1;
        taskCountsPerMonth[month] = (taskCountsPerMonth[month] || 0) + 1;
      });
      return (
        <div>
          <div>
            {taskCountsPerMonth[month] ? (
              <div className="flex justify-end">
                <span className="ml-1 text-gray-500 ">
                  ({taskCountsPerMonth[month]} task)
                </span>
              </div>
            ) : (
              ""
            )}
          </div>
          <ul className="events">
            {tasksForMonth.map((task) => (
              <div
                onClick={() => {
                  showModal(task);
                  setdataEdit({
                    id: task.id,
                    name: task.name,
                    status: task.status,
                    deadStart: task.deadStart,
                    deadEnd: task.deadEnd,
                    deadline: task.deadline,
                    priority: task.priority,
                    point: task.point,
                    users: task.users,
                  });
                }}
                key={task.id}
              >
                <Dropdown menu={{ items }} placement="bottomRight" arrow>
                  <p
                    key={task.id}
                    onMouseEnter={() => {
                      handleMouseEnter(task.users, task.id);
                    }}
                    onMouseLeave={handleMouseLeave}
                    // onClick={() => {
                    //   router.push(`/detailTask/${task.id}`);
                    // }}
                    className={`border-2 p-1 text-xs m-0.5 rounded-md flex justify-between mb-1 text-black  ${
                      task.status == Status.DONE
                        ? "border-green-500 hover:bg-green-100"
                        : task.status == Status.PROCESSING
                        ? "border-yellow-500 hover:bg-yellow-100"
                        : "border-red-500 hover:bg-red-100"
                    }`}
                  >
                    <p>{task.name}</p>
                    <p>({task.users.length} assignes)</p>
                  </p>
                </Dropdown>
              </div>
            ))}
          </ul>
        </div>
      );
    }
    if (query.mode == "month" || !query.mode) {
      const tasksForDate = tasks.filter(
        (task) =>
          task.deadStart?.split("T")[0] === dateString ||
          task.deadEnd?.split("T")[0] === dateString
      );

      let check = tasksForDate.find((value) => value.id === itemHover);
      if (check) {
        const isTaskDate = tasks.some((task) => {
          const taskStartDate = task.deadStart?.split("T")[0];
          const taskEndDate = task.deadEnd?.split("T")[0];
          if (taskStartDate && taskEndDate) {
            return dateString >= taskStartDate && dateString <= taskEndDate;
          }
        });

        if (isTaskDate) {
          const tdElements = document.querySelectorAll(
            `td[title="${dayjs(value).format("YYYY-MM-DD")}"]`
          );
          tdElements.forEach((tdElement) => {
            const element = tdElement as HTMLElement;
            element.style.background = `  ${
              check?.status == Status.PROCESSING
                ? "#fdffec"
                : check?.status == Status.DONE
                ? "#ecfff3"
                : "#ffecec"
            }`;
          });
        }
      }

      return (
        <ul className="events">
          {tasksForDate.map((task) => {
            return (
              <div
                onClick={() => {
                  showModal(task);
                  setdataEdit({
                    id: task.id,
                    name: task.name,
                    status: task.status,
                    deadStart: task.deadStart,
                    deadEnd: task.deadEnd,
                    deadline: task.deadline,
                    priority: task.priority,
                    point: task.point,
                    users: task.users,
                  });
                }}
                key={task.id}
              >
                <Dropdown menu={{ items }} placement="bottomRight" arrow>
                  <p
                    key={task.id}
                    onMouseEnter={() => {
                      handleMouseEnter(task.users, task.id);
                    }}
                    onMouseLeave={handleMouseLeave}
                    className={`border-2 p-1 text-xs m-0.5 rounded-md flex justify-between mb-1 text-black  ${
                      task.status == Status.DONE
                        ? "border-green-500 hover:bg-green-100"
                        : task.status == Status.PROCESSING
                        ? "border-yellow-500 hover:bg-yellow-100"
                        : "border-red-500 hover:bg-red-100"
                    }`}
                  >
                    <p>{task.name}</p>
                    <p>
                      {dayjs(dayjs(value).format("YYYY-MM-DD")) >=
                        dayjs(dayjs(task.deadStart).format("YYYY-MM-DD")) &&
                      dayjs(dayjs(value).format("YYYY-MM-DD")) <
                        dayjs(dayjs(task.deadEnd).format("YYYY-MM-DD"))
                        ? "Start"
                        : "End"}
                    </p>
                  </p>
                </Dropdown>
              </div>
            );
          })}
        </ul>
      );
    }
  };

  const items: MenuProps["items"] =
    dataassigne && dataassigne.length > 0
      ? dataassigne.map((item) => ({
          key: item.user.id.toString(),
          label: <div rel="noopener noreferrer">{item.user.username}</div>,
        }))
      : [
          {
            key: "123123",
            label: <div rel="noopener noreferrer">My task</div>,
          },
        ];

  const onPanelChange = (date: Dayjs, mode: string) => {
    if (mode == "month") {
      const startOfMonth = date.startOf("month");
      const endOfMonth = date.endOf("month");
      const firstCellDate = startOfMonth.subtract(startOfMonth.day(), "day");
      const duration = endOfMonth.diff(firstCellDate, "day");
      const lastCellDate = endOfMonth.add(6 - endOfMonth.day(), "day");
      setoptionTable({
        duration: duration,
        end: endOfMonth.toString(),
        tableEnd: lastCellDate.format("YYYY-MM-DDTHH:mm:ss.SSSZ").toString(),
      });

      const firstDayOfMonth = dayjs(date);
      const monthValue = firstDayOfMonth.month(); // Truy cập giá trị tháng
      router.push({
        query: {
          ...query,
          mode: mode,
          firstCellDate: firstCellDate
            .startOf("day")
            .format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
          lastCellDate: lastCellDate
            .endOf("day")
            .format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
          currentMonth: monthValue,
          currentDates: date.format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
        },
      });
    } else {
      const startOfYear = dayjs()
        .year(dayjs(date.format("YYYY-MM-DDTHH:mm:ss.SSSZ")).year())
        .startOf("year");
      const endOfYear = dayjs()
        .year(dayjs(date.format("YYYY-MM-DDTHH:mm:ss.SSSZ")).year())
        .endOf("year");

      router.push({
        query: {
          ...query,
          mode: mode,
          firstCellDate: startOfYear.format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
          lastCellDate: endOfYear.format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
          currentYear: dayjs(date).year(),
          currentDates: date.format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
        },
      });
    }
  };

  const handleChangeStatus = (value: string[]) => {
    setdataFilter({ ...dataFilter, status: JSON.stringify(value) });
  };

  const handleChangeDeadline = (value: string) => {
    setdataFilter({ ...dataFilter, deadline: JSON.stringify(value) });
  };

  const handleChangePoint = (value: string) => {
    setdataFilter({ ...dataFilter, point: JSON.stringify(value) });
  };

  const handleChangePriority = (value: string) => {
    setdataFilter({ ...dataFilter, priority: JSON.stringify(value) });
  };

  const handleChangeAssinges = (value: string) => {
    if (value == "none") {
      setdataFilter({ ...dataFilter, assigned: null });
    } else {
      setdataFilter({ ...dataFilter, assigned: value });
    }
  };

  const handleGetAllOption = () => {
    setdataFilter({
      status: null,
      point: null,
      priority: null,
      deadline: null,
      assigned: null,
      doeDateStart: null,
      doeDateend: null,
    });
    onClose();
    router.push({
      pathname: "/calender",
      query: {},
    });
  };

  const optionsStatus: SelectProps["options"] = [];
  filterOptionStatus2.map((value) => {
    optionsStatus.push({
      label: value.lable,
      value: value.key,
    });
  });

  const optionsDeadline: SelectProps["options"] = [];
  filterOptionDeadline.map((value) => {
    optionsDeadline.push({
      label: value.lable,
      value: value.key,
    });
  });

  const optionsPoint: SelectProps["options"] = [];
  filterOptionPoint.map((value) => {
    optionsPoint.push({
      label: value.lable,
      value: value.key,
    });
  });

  const optionsPriority: SelectProps["options"] = [];
  filterOptionPriority.map((value) => {
    optionsPriority.push({
      label: value.lable,
      value: value.key,
    });
  });

  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const handleclose = () => {
    console.log({ ...dataFilter });
    router.push({
      pathname: `/calender`,
      query: { ...dataFilter },
    });
    onClose();
  };

  const [isModalOpenDetail, setIsModalOpenDetail] = useState(false);
  const [dataDetail, setDataDetail] = useState<IDetailProps>();
  const showModal = (data: IDetailProps) => {
    setDataDetail(data);
    setIsModalOpenDetail(true);
  };

  const handleOkDetail = () => {
    setIsModalOpenDetail(false);
  };

  const handleCancelDetail = () => {
    setIsModalOpenDetail(false);
  };

  const dateNow = new Date(Date.now());
  let messages = dataDetail?.users.find(value => value.userID == session?.user?.name)

  const { RangePicker } = DatePicker;
  type RangeValue = Parameters<
    NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>["onChange"]>
  >[0];

  const [doeDate, setDoeDate] = useState({ doeDateStart: "", doeDateEnd: "" });

  const handleDoeDateStart: DatePickerProps["onChange"] = (
    date,
    dateString
  ) => {
    setDoeDate({ ...doeDate, doeDateStart: dateString.toString() });
    setdataFilter({
      ...dataFilter,
      doeDateStart: dateString.toString(),
    });
  };

  const handleDoeDateEnd: DatePickerProps["onChange"] = (date, dateString) => {
    setDoeDate({ ...doeDate, doeDateEnd: dateString.toString() });
    setdataFilter({
      ...dataFilter,
      doeDateend: dateString.toString(),
    });
  };
  const dateFormat = "YYYY-MM-DD HH:mm:ss";
  const dateFormats = "YYYY-MM-DD";

  const [dataEdit, setdataEdit] = useState<IPageProps>();

  const handleChangleEdit = (event: React.ChangeEvent<HTMLInputElement>) => {
    if(event.target.value.length !== 0 ){
      dispatch(setEditToggle(true));
      let news = { ...dataEdit, name: event.target.value } as IPageProps;
      setdataEdit(news);
    }else{
      message.warning('khong hop le')
      let news = { ...dataEdit, name: event.target.value } as IPageProps;
      setdataEdit(news);
    }
   
  };

  const options = [
    { label: "Todo", value: Status.TODO },
    { label: "Processing", value: Status.PROCESSING },
    { label: "Done", value: Status.DONE },
  ];
  const dateNows = new Date(Date.now());

  const handleChangeDate = (a: RangeValue, b: string[]) => {
    dispatch(setEditToggle(true));
    dayjs.extend(utc);
    let start = b[0] == "" ? Date.now() : b[0];
    let end = b[1] == "" ? Date.now() : b[1];
    if (dataEdit) {
      let newdata = {
        ...dataEdit,
        deadStart: new Date(start).toISOString(),
        deadEnd: new Date(end).toISOString(),
        deadline:
          new Date(start) < dateNows && new Date(end) > dateNows
            ? DeadLine.RUNNING
            : new Date(start) > dateNows
            ? DeadLine.COMING
            : new Date(end) < dateNows
            ? DeadLine.OVER
            : undefined,
      };
      setdataEdit(newdata);
    }
  };

  const handleCancelEdit = () => {
    dispatch(setEditToggle(false));
    if (dataDetail) {
      setdataEdit({
        id: dataDetail.id,
        name: dataDetail.name,
        status: dataDetail.status,
        deadStart: dataDetail.deadStart,
        deadEnd: dataDetail.deadEnd,
        deadline: dataDetail.deadline as DeadLine,
        priority: dataDetail.priority,
        point: dataDetail.point,
        users: [],
      });
    }
  };
  const [editloading, setEditLoading] = useState(false);

  const handleEditReq = async () => {
    try {
      let dataNewChange = dataEdit as IPageProps;
      if (dataNewChange.name.length !== 0) {
        dispatch(setEditToggle(false));
        setEditLoading(true);
        let data = [...dataTasks];
        let index = data.findIndex((value) => value.id == dataNewChange.id);
        if (index !== -10) {
          data[index] = { ...data[index], ...dataNewChange };
          dispatch(setTasks(data));
          dispatch(setdataCalender(data));
          await axiosConfig.put(`/api/task/${dataNewChange.id}`, dataNewChange);
          setEditLoading(false);
          message.success("Đã sửa thành công");
          dispatch(setTasksBackUp(data));
        }
      }else{
        message.warning('task khong hop le ')
      }
    } catch (error) {
      dispatch(setTasks([...dataTasksBackUp]));
      message.error("loi r ban oi");
    }
  };
  const [valAssigne, setValAssigne] = useState("");
  const [valMessageAssigned, setvalMessageAssigned] = useState("");

  const handleChangeValAssigne = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setValAssigne(event.target.value);
  };

  const handleAssigned = async () => {
    dispatch(setloading(true));
    let checked = await axiosConfig.post(`/api/userontask`, {
      email: valAssigne,
      taskID: dataDetail?.id,
      message: valMessageAssigned ? valMessageAssigned : "none!",
    });
    if (checked.data.message == "đã thêm trước đó") {
      dispatch(setloading(false));
      setValAssigne("");
      setvalMessageAssigned("");
      return message.warning(checked.data.message);
    }

    const currentDate = new Date();
    const currentMonth = query.currentMonth
      ? query.currentMonth
      : currentDate.getMonth();
    let year = dayjs(query.currentDates?.toString()).year();

    const startOfMonth = dayjs().startOf("month");
    const endOfMonth = dayjs(`${year}-${Number(currentMonth) + 1}`).endOf(
      "month"
    );

    const firstCellDate = query.firstCellDate
      ? dayjs(query.firstCellDate.toString())
      : startOfMonth.subtract(startOfMonth.day(), "day");

    const lastCellDate = query.lastCellDate
      ? dayjs(query.lastCellDate.toString())
      : endOfMonth.add(6 - endOfMonth.day(), "day");

    setCurrentDate(query.currentDates);
    setselect(query);

    let data = await axiosConfig.get("/api/task", {
      params: {
        ...query,
        currentMonth,
        firstCellDate: !query.firstCellDate
          ? firstCellDate.subtract(1, "day").format("YYYY-MM-DD")
          : query.firstCellDate,
        lastCellDate: !query.lastCellDate
          ? lastCellDate.add(1, "day").format("YYYY-MM-DD")
          : query.lastCellDate,
      },
    });

    dispatch(setTasks(data.data.datas));
    dispatch(setdataCalender(data.data.datas));
    setDataDetail(
      data.data.datas.find((value: IPageProps) => value.id == dataDetail?.id)
    );
    dispatch(setloading(false));
    message.success(checked.data.message);
    setValAssigne("");
    setvalMessageAssigned("");
  };

  const handleDisconnects = async (IDDisconnect: string) => {
    dispatch(setloading(true));
    await axiosConfig.patch(`/api/userontask`, {
      userID: IDDisconnect,
      taskID: dataEdit?.id,
    });

    const currentDate = new Date();
    const currentMonth = query.currentMonth
      ? query.currentMonth
      : currentDate.getMonth();

    dispatch(setloading(true));

    let year = dayjs(query.currentDates?.toString()).year();

    const startOfMonth = dayjs().startOf("month");
    const endOfMonth = dayjs(`${year}-${Number(currentMonth) + 1}`).endOf(
      "month"
    );

    const firstCellDate = query.firstCellDate
      ? dayjs(query.firstCellDate.toString())
      : startOfMonth.subtract(startOfMonth.day(), "day");

    const lastCellDate = query.lastCellDate
      ? dayjs(query.lastCellDate.toString())
      : endOfMonth.add(6 - endOfMonth.day(), "day");

    setCurrentDate(query.currentDates);
    setselect(query);

    let data = await axiosConfig.get("/api/task", {
      params: {
        ...query,
        currentMonth,
        firstCellDate: !query.firstCellDate
          ? firstCellDate.subtract(1, "day").format("YYYY-MM-DD")
          : query.firstCellDate,
        lastCellDate: !query.lastCellDate
          ? lastCellDate.add(1, "day").format("YYYY-MM-DD")
          : query.lastCellDate,
      },
    });
    dispatch(setTasks(data.data.datas));
    dispatch(setdataCalender(data.data.datas));
    setDataDetail(
      data.data.datas.find((value: IPageProps) => value.id == dataEdit?.id)
    );
    dispatch(setloading(false));
    message.success("Đã gỡ thành công");
  };

  const handleChangeMessage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setvalMessageAssigned(event.target.value);
  };

  const [toggleTask, setToggleTask] = useState(false);

  const handleShowHideTask = () => {
    setToggleTask(!toggleTask);
  };

  const validateInputPriorty = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setEditToggle(true));
    const value = Number(event.target.value);
    if (value < 0 || value > 5) {
      event.target.value = ""; // Xóa giá trị nhập nếu nằm ngoài khoảng
    } else {
      let newdata = { ...dataEdit, priority: value } as IPageProps;
      setdataEdit(newdata);
    }
  };

  const validateInputPoint = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setEditToggle(true));
    const value = Number(event.target.value);
    if (value < 0 || value > 10) {
      event.target.value = ""; // Xóa giá trị nhập nếu nằm ngoài khoảng
    } else {
      let newdata = { ...dataEdit, point: value } as IPageProps;
      setdataEdit(newdata);
    }
  };


  return (
    <div className="relative w-full h-screen grid grid-cols-5 overflow-hidden	">
      <Modal
        width={800}
        className="custom-modal"
        open={isModalOpenDetail}
        onOk={handleOkDetail}
        footer={null}
        closable={false}
        okButtonProps={{ style: { display: "none" } }}
        cancelButtonProps={{ style: { display: "none" } }}
        onCancel={handleCancelDetail}
      >
        <div className="flex items-center justify-center">
          <div
            className={`flex flex-col  border  w-full h-full bg-white rounded-md  shadow-2xl	shadow-black-500/50 p-4 ${
              dataDetail?.status == Status.DONE
                ? " shadow-green-500/50 border-green-900"
                : dataDetail?.status == Status.PROCESSING
                ? " shadow-yellow-500/50 border-yellow-900"
                : "shadow-red-500/50 border-red-900"
            }`}
          >
            <h1 className="mb-2 flex justify-between items-center">
              <div>
                <span className="text-3xl">{dataEdit?.name} </span>
                <span className=" bottom-50 left-24 text-sm text-black shadow-xl bg-white w-22 px-3 py-1 rounded  ">
                  {new Date(dataEdit?.deadStart ? dataEdit.deadStart : "") <=
                    dateNow &&
                  new Date(dataEdit?.deadEnd ? dataEdit.deadEnd : "") >=
                    dateNow ? (
                    <span className="text-yellow-500">Đang diễn ra</span>
                  ) : new Date(dataEdit?.deadStart ? dataEdit.deadStart : "") <
                    dateNow ? (
                    <span className="text-red-500">Đã hết hạn</span>
                  ) : !dataEdit?.deadStart && !dataEdit?.deadEnd ? (
                    ""
                  ) : (
                    <span className="text-green-500">Sắp tới</span>
                  )}
                </span>
              </div>

              {editloading ? <Spin></Spin> : ""}

              {editToggle ? (
                <div>
                  {" "}
                  <button
                    onClick={handleCancelEdit}
                    className="border rounded-md py-1 px-4 text-sm hover:bg-black hover:text-white"
                  >
                    hủy
                  </button>{" "}
                  <button
                    onClick={handleEditReq}
                    className="border rounded-md bg-red-500 hover:bg-red-200 hover:!text-red-500 p-1 px-2 text-sm text-white"
                  >
                    Xác nhận
                  </button>{" "}
                </div>
              ) : (
                ""
              )}
            </h1>

            <div className=" w-full h-full grid grid-cols-2 gap-2">
              <div className=" shadow-lg">
                <h1>Details</h1>
                <div>
                  <ul>
                    <li className="border flex group/item">
                      <div className="w-24  p-4 border-r">Task:</div>{" "}
                      <div className=" p-4 flex justify-between">
                        <p>
                          <input
                            className="ml-2 underline-offset-8 border border-gray-100 p-1 rounded-md w-full"
                            type="text"
                            onChange={handleChangleEdit}
                            value={dataEdit?.name}
                          />
                        </p>{" "}
                      </div>{" "}
                    </li>
                    <li className="border flex ">
                      <div className="w-24 p-4 border-r ">Author:</div>{" "}
                      <div className=" p-4">{dataDetail?.author?.email}</div>
                    </li>
                    <li className="border flex  group/item">
                      <div className="w-24  p-4 border-r">Status:</div>{" "}
                      <div className=" p-4 flex justify-between">
                        <p>
                          <p className="w-full ml-2">
                            <RadioForm
                              options={options}
                              setdataEdit={setdataEdit}
                              dataEdit={dataEdit}
                            />
                          </p>
                        </p>{" "}
                      </div>{" "}
                    </li>
                    <li className="border flex ">
                      <div className="w-24  p-4 border-r">DeadLine:</div>{" "}
                      <div className=" p-4">
                        <p>{dataEdit?.deadline}</p>{" "}
                      </div>{" "}
                    </li>
                    <li className="border flex  group/item">
                      <div className="w-24  p-4 border-r">priority:</div>{" "}
                      <div className=" p-4 flex justify-between">
                        <p className="w-full">
                          {" "}
                          <div className="w-full ml-4 ">
                            <input
                              type="number"
                              min="0"
                              max="5"
                              value={dataEdit?.priority}
                              onChange={validateInputPriorty}
                            />
                          </div>
                        </p>
                      </div>{" "}
                    </li>
                    <li className="border flex  group/item">
                      <div className="w-24  p-4 border-r">Point:</div>{" "}
                      <div className=" p-4 flex justify-between">
                        <p className="w-full">
                          <div className="w-full ml-4  ">
                            <input
                              type="number"
                              min="0"
                              max="10"
                              value={dataEdit?.point}
                              onChange={validateInputPoint}
                            />
                          </div>
                        </p>
                      </div>{" "}
                    </li>
                    <li className="border flex  group/item">
                      <div className="w-24  p-4 border-r">Date:</div>{" "}
                      <div className=" p-4 flex items-center justify-between">
                        <div className="w-full ">
                          <Space direction="vertical" size={12}>
                            <RangePicker
                              className="flex flex-col border-none items-center justify-center"
                              showTime
                              onChange={handleChangeDate}
                              value={[
                                dayjs(
                                  `${
                                    dataEdit?.deadStart == dateNow.toString()
                                      ? dateNow
                                      : dayjs(dataEdit?.deadStart)
                                          .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
                                          .toString()
                                  }`,
                                  dateFormat
                                ),
                                dayjs(
                                  `${
                                    dataEdit?.deadEnd == dateNow.toString()
                                      ? dateNow
                                      : dayjs(dataEdit?.deadEnd)
                                          .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
                                          .toString()
                                  }`,
                                  dateFormat
                                ),
                              ]}
                            />
                          </Space>
                        </div>
                      </div>{" "}
                    </li>

                    {session?.user?.name !== dataDetail?.author?.id ? (
                      <li className="border flex">
                        <div className="w-24  p-4 border-r">Message:</div>{" "}
                        <div className=" p-4">{messages?.message}</div>
                      </li>
                    ) : (
                      ""
                    )}
                  </ul>
                </div>
              </div>
              <div className=" shadow-lg px-4">
                <h1>Assignes {`(${dataDetail?.users.length})`}</h1>
                <div className="">
                  {session?.user?.name == dataDetail?.author?.id ? (
                    <div>
                      <div className="flex my-4">
                        <Input
                          disabled={
                            session?.user?.name !== dataDetail?.author?.id
                              ? true
                              : false
                          }
                          value={valAssigne}
                          onChange={handleChangeValAssigne}
                          className="mr-2"
                          placeholder="Email"
                        />
                        <Button
                          disabled={
                            session?.user?.name !== dataDetail?.author?.id
                              ? true
                              : false
                          }
                          onClick={handleAssigned}
                        >
                          add
                        </Button>
                      </div>
                      <Input
                        showCount
                        maxLength={20}
                        disabled={
                          session?.user?.name !== dataDetail?.author?.id
                            ? true
                            : false
                        }
                        value={valMessageAssigned}
                        onChange={handleChangeMessage}
                        placeholder="Message......"
                        // autoSize={{ minRows: 2, maxRows: 6 }}
                      />
                    </div>
                  ) : (
                    ""
                  )}
                  <ul>
                    {dataDetail?.users.map((value) => {
                      return (
                        <li
                          key={value.userID}
                          className="mt-4 flex items-end relative"
                        >
                          <Avatar
                            style={{
                              backgroundColor: "#fde3cf",
                              color: "#f56a00",
                            }}
                            className="mr-2 px-4"
                          >
                            {value.user.username.charAt(0).toLocaleUpperCase()}
                          </Avatar>
                          <div>
                            <div> {value.user.email}</div>
                            <div className="text-xs">
                              {" "}
                              {value.user.username}
                            </div>
                          </div>
                          {session?.user?.name == dataDetail.author?.id ?  <span className="text-xs">message: {value.message}</span> : ''}
                         
                          <p className="absolute top-0 right-0 ">
                            <span className="text-xs">
                              {new Date(
                                value.assignedAt ? value.assignedAt : Date.now()
                              ).toLocaleTimeString()}
                            </span>
                            <span className="text-xs">
                              {new Date(
                                value.assignedAt ? value.assignedAt : Date.now()
                              ).toLocaleDateString()}
                            </span>
                          </p>
                          {session?.user?.name == dataDetail?.author?.id ||
                          session?.user?.name == value.user.id ? (
                            <div
                              onClick={() => {
                                handleDisconnects(value.user.id);
                              }}
                              className="  group-hover/item:opacity-100 text-orange-800  absolute bottom-0 right-0 hover:cursor-pointer hover:text-red-500 text-xl "
                            >
                              {session?.user?.name !== value.user.id ? (
                                <div>
                                  <AiOutlineDelete className="text-base"></AiOutlineDelete>
                                </div>
                              ) : (
                                <div>
                                  <LogoutOutlined className="text-base" />
                                </div>
                              )}
                            </div>
                          ) : (
                            ""
                          )}
                        </li>
                      );
                    })}
                  </ul>
                  {loading ? (
                    <div className="flex mt-12 items-center justify-center">
                      <Loading type="points" />
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      {loading ? (
        <div className="absolute w-full h-full bg-gray-300 top-0 left-0 z-50 opacity-50 flex items-center justify-center ">
          <Spin className="!opacity-100" indicator={antIcon} />
        </div>
      ) : (
        ""
      )}
      {toggleTask ? (
        <div className=" relative task">
          <div className="h-screen overflow-auto  scrollbar-thin scrollbar-thumb-gray-300 scrollbar-track-gray-100">
            {toggleTask ? (
              <ul id="taskList">
                {dataCalender?.length == 0 ? (
                  <div className="h-96 flex items-center justify-center">
                    <Empty />
                  </div>
                ) : (
                  ""
                )}
                {dataCalender?.map((value) => {
                  return (
                    <li
                      className=""
                      key={value.id}
                      onClick={() => {
                        router.push(`/detailTask/${value.id}`);
                      }}
                    >
                      <div
                        className={` border-2  cursor-pointer${
                          value.status == Status.DONE
                            ? " 	hover:bg-green-100"
                            : value.status == Status.PROCESSING
                            ? " 	hover:bg-yellow-100"
                            : "	hover:bg-red-100"
                        } p-3 m-2 rounded  
                ${
                  value.status == Status.DONE
                    ? "border-green-500"
                    : value.status == Status.PROCESSING
                    ? "border-yellow-500"
                    : "border-red-500"
                }`}
                      >
                        <div className="flex  justify-between flex-wrap">
                          <p>{value.name}</p>
                          <Tag
                            color={
                              value.status == Status.DONE
                                ? "green"
                                : value.status == Status.PROCESSING
                                ? "gold"
                                : "red"
                            }
                          >
                            {value.status.toLocaleLowerCase()}
                          </Tag>
                        </div>
                        <div className="flex text-xs mt-2 flex-wrap">
                          <p className="mr-4">
                            <span>Priority: </span>
                            {value.priority == 0 ? "Chưa có" : value.priority}
                          </p>
                          <p className="mr-4">
                            <span>Point: </span>
                            {value.point == 0 ? "Chưa có" : value.point}
                          </p>
                          <p>
                            <span>Start: </span>{" "}
                            {dayjs(value.deadStart?.toString()).format(
                              "DD/MM/YYYY"
                            )}
                          </p>
                        </div>
                      </div>
                    </li>
                  );
                })}
              </ul>
            ) : (
              ""
            )}
          </div>
          <div className="w-0.5 h-full bg-gray-200 absolute top-0 right-0"></div>
        </div>
      ) : (
        ""
      )}

      <div
        className={`pt-4 col-span-4  max-h-screen ${
          !toggleTask ? "w-screen" : ""
        }`}
      >
        <div className="flex items-start justify-between mr-2">
          <h1 className="flex items-center relative  w-full justify-start flex-wrap">
            <div className="flex items-center justify-between w-ful mr-4 flex-wrap">
              <Avatar
                style={{ backgroundColor: "#fde3cf", color: "#f56a00" }}
                className="mr-2 px-4 ml-2"
              >
                {session && session?.user?.email
                  ? JSON.parse(session.user.email)
                      .username.charAt(0)
                      .toLocaleUpperCase()
                  : ""}
              </Avatar>
              <div className="m-1">
                {" "}
                {session && session?.user?.email
                  ? JSON.parse(session.user.email).username.toLocaleUpperCase()
                  : ""}
              </div>
            </div>

            <div
              onClick={() => {
                router.push(`/homepage`);
              }}
            >
              <AiOutlineRollback className="text-2xl cursor-pointer	hover:text-3xl"></AiOutlineRollback>
            </div>

            <div className="flex absolute -bottom-10 z-50 left-0 ml-4 items-center justify-between">
              <p className="flex items-center justify-end  text-sm mr-4 underline decoration-solid decoration-gray-500 decoration-2 ">
                {dataCalender?.length} tasks{" "}
              </p>
              <button
                onClick={handleShowHideTask}
                className="ml-4 text-xs cursor-pointer text-gray-500 hover:text-blue-500"
              >
                {!toggleTask ? "show task" : "hide task"}
              </button>
            </div>
          </h1>

          <div className="ml-4 flex text-xs text-slate-500  w-96	flex-wrap">
            {Object.keys(query ?? {}).map((key) => {
              if (
                key !== "homepage" &&
                key !== "homeID" &&
                key !== "currentDates" &&
                key !== "currentMonth" &&
                key !== "firstCellDate" &&
                key !== "lastCellDate" &&
                key !== "mode" &&
                key !== "page" &&
                key !== "currentYear"
              ) {
                if (
                  key == "assigned" ||
                  key == "name" ||
                  key == "mode" ||
                  key == "currentYear" ||
                  key == "doeDateStart" ||
                  key == "doeDateend"
                ) {
                  return (
                    <p key={key} className="mr-1 pb-2">
                      {query[key] ? (
                        <>
                          <span>
                            {key == "doeDateStart"
                              ? "From"
                              : key == "doeDateend"
                              ? "To"
                              : key}
                            :
                          </span>{" "}
                          <span className="border rounded-md p-0.5">
                            {key == "doeDateStart" || key == "doeDateend"
                              ? dayjs(query[key]?.toString()).format(
                                  "DD-MM-YYYY"
                                )
                              : query[key]}
                          </span>{" "}
                          /
                        </>
                      ) : (
                        ""
                      )}
                    </p>
                  );
                }
                if (key == "point" || key == "priority") {
                  return (
                    <p key={key} className="mr-1 pb-2">
                      {JSON.parse(query[`${key}`]?.toString() ?? "").length >
                      0 ? (
                        <>
                          <span>{key}:</span>{" "}
                          <span className="border rounded-md p-0.5">
                            {JSON.parse(query[`${key}`]?.toString() ?? "").join(
                              " _ "
                            )}
                          </span>{" "}
                          /
                        </>
                      ) : (
                        ""
                      )}
                    </p>
                  );
                }
                return (
                  <p key={key} className="mr-1 pb-2">
                    {JSON.parse(query[`${key}`]?.toString() ?? "").length >
                    0 ? (
                      <>
                        <span>{key}:</span>{" "}
                        <span className="border rounded-md p-0.5">
                          {JSON.parse(query[`${key}`]?.toString() ?? "").join(
                            " - "
                          )}
                        </span>{" "}
                        /
                      </>
                    ) : (
                      ""
                    )}
                  </p>
                );
              }
            })}
          </div>
          <div className="flex ">
            <label className="relative block ">
              <span className="sr-only">Search</span>
              <span className="absolute inset-y-0 left-0 flex items-center pl-2">
                <SearchOutlined />
              </span>
              <input
                onKeyDown={handleNameSearch}
                onChange={handleChangeNameSearch}
                value={nameSearch}
                className="placeholder:italic placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-1.5 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
                placeholder="Search for anything..."
                type="text"
                name="search"
              />
            </label>
            <>
              <Button
                className="text-black ml-2 border flex items-center justify-center"
                onClick={showDrawer}
              >
                <img
                  className="w-4 h-4 mr-1 opacity-50 -rotate-90 "
                  src="https://cdn.icon-icons.com/icons2/1673/PNG/512/options2outline_110906.png"
                  alt=""
                />
                Filters
              </Button>
              <Drawer
                title="Filters"
                placement="right"
                onClose={onClose}
                open={open}
              >
                <div className="w-11/12 pb-4  ">
                  <h1 className="mb-2">Due date:</h1>
                  <div className="flex">
                    <DatePicker
                      className="mr-2"
                      placeholder="date start"
                      value={
                        doeDate.doeDateStart
                          ? dayjs(doeDate.doeDateStart, dateFormats)
                          : null
                      }
                      onChange={handleDoeDateStart}
                    />
                    <DatePicker
                      placeholder="date end"
                      value={
                        doeDate.doeDateEnd
                          ? dayjs(doeDate.doeDateEnd, dateFormats)
                          : null
                      }
                      onChange={handleDoeDateEnd}
                    />
                  </div>
                </div>
                <div className="w-11/12 pb-4">
                  <h1 className="mb-2">Status:</h1>
                  <Select
                    mode="multiple"
                    allowClear
                    style={{ width: "100%" }}
                    placeholder="status"
                    value={
                      dataFilter.status
                        ? JSON.parse(dataFilter.status.toString())
                        : []
                    }
                    onChange={handleChangeStatus}
                    options={optionsStatus}
                  />
                </div>
                <div className="w-11/12 pb-4">
                  <h1 className="mb-2">Deadline:</h1>
                  <Select
                    mode="multiple"
                    allowClear
                    style={{ width: "100%" }}
                    placeholder="Deadline"
                    value={
                      dataFilter.deadline
                        ? JSON.parse(dataFilter.deadline.toString())
                        : []
                    }
                    onChange={handleChangeDeadline}
                    options={optionsDeadline}
                  />
                </div>
                <div className="w-11/12 pb-4">
                  <h1 className="mb-2">Point:</h1>
                  <Select
                    mode="multiple"
                    allowClear
                    style={{ width: "100%" }}
                    placeholder="Point"
                    value={
                      dataFilter.point
                        ? JSON.parse(dataFilter.point.toString())
                        : []
                    }
                    onChange={handleChangePoint}
                    options={optionsPoint}
                  />
                </div>
                <div className="w-11/12 pb-4">
                  <h1 className="mb-2">Priority:</h1>
                  <Select
                    mode="multiple"
                    allowClear
                    style={{ width: "100%" }}
                    placeholder="Priority"
                    value={
                      dataFilter.priority
                        ? JSON.parse(dataFilter.priority.toString())
                        : []
                    }
                    onChange={handleChangePriority}
                    options={optionsPriority}
                  />
                </div>
                <div className="pb-4">
                  <h1 className="mb-2">Assinged:</h1>
                  <Select
                    value={
                      dataFilter.assigned
                        ? dataFilter.assigned.toString()
                        : "none"
                    }
                    style={{ width: 120 }}
                    onChange={handleChangeAssinges}
                    options={[
                      { label: "none", value: "none" },
                      { label: "My tasks", value: "mytask" },
                      { label: "Assigness", value: "assignes" },
                    ]}
                  />
                </div>
                <div className="mt-4">
                  <Button
                    className="w-28 inline-flex	 justify-center text-black bg-white hover:!border-black hover:!text-white hover:bg-black"
                    onClick={handleGetAllOption}
                  >
                    GetAll
                  </Button>
                </div>
                <div className="mt-16">
                  <Button
                    onClick={handleclose}
                    className="py-6 text-xl flex justify-center items-center w-full border-2  border hover:bg-black hover:!text-white hover:!border-black"
                  >
                    TÌm kiếm
                  </Button>
                </div>
              </Drawer>
            </>
          </div>
        </div>
        <div className="w-full heightcalender flex  items-end flex-col  bg-white relative  overflow-auto scrollbar-thin scrollbar-thumb-gray-300 scrollbar-track-gray-100">
          <div className="w-full max-h-full overflow-auto  scrollbar-thin scrollbar-thumb-gray-300 scrollbar-track-gray-100	">
            <Calendar
              cellRender={dateCellRender}
              value={currentDates ? dayjs(currentDates.toString()) : dayjs()}
              onPanelChange={onPanelChange}
              mode={query.mode ? (query.mode as CalendarMode) : "month"}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Calenders;
