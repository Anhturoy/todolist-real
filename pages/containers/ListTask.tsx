import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import {
  AiOutlineClose,
  AiOutlineCheck,
  AiOutlinePause,
  AiOutlineUserAdd,
} from "react-icons/ai";
import { IPageProps } from "../index";
import { DatePicker, DatePickerProps, MenuProps, SelectProps } from "antd";
import { Avatar, Dropdown, List, Select, Spin, message } from "antd";
import {
  SwapRightOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import { Pagination } from "antd";
import { useRouter } from "next/router";
import { Empty } from "antd";
import { usePathname } from "next/navigation";
import { Button, Drawer } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { setTasks, setloading } from "@/redux/counterSlice";
import { useSession } from "next-auth/react";
import { Input } from "antd";
import axiosConfig from "@/axiosConfig/axiosConfig";
import dayjs from "dayjs";
import {
  filterOptionDeadline,
  filterOptionPoint,
  filterOptionPriority,
  filterOptionStatus2,
} from "@/option";
import { DataFilters } from "./Calender";
// import { DataFilters } from "../[homepage]/calender";

const { TextArea } = Input;
interface ListTaskProps {
  openFilter: boolean;
  handleChangeStatus: (status: Status) => void;
  showModal: (value: IPageProps) => void;
  handleDelete: (id: string) => void;
  setIdChange: Dispatch<SetStateAction<string>>;
  amount: number;
  setAmount: Dispatch<SetStateAction<number>>;
  setCurrent: Dispatch<SetStateAction<number>>;
  current: number;
  setOpenFilter: Dispatch<SetStateAction<boolean>>;
}

enum Status {
  TODO = "TODO",
  PROCESSING = "PROCESSING",
  DONE = "DONE",
}

const ListTask: React.FC<ListTaskProps> = ({
  setOpenFilter,
  openFilter,
  handleChangeStatus,
  showModal,
  handleDelete,
  setIdChange,
  amount,
  setCurrent,
  current,
}) => {
  const counter = useSelector(
    (state: {
      counter: {
        loading: boolean;
        dataEdit: IPageProps;
        dataTasks: IPageProps[];
        deleLoading: boolean;
        createLoading: boolean;
        imageUrl: string;
      };
    }) => state.counter
  );
  const { dataTasks, loading, deleLoading, createLoading, imageUrl } = counter;
  const dateNow = new Date(Date.now());
  let items: MenuProps["items"] = [
    {
      label: (
        <p
          className="text-red-500 font-medium	flex justify-flex-start items-center"
          onClick={() => {
            handleChangeStatus(Status.TODO);
          }}
        >
          <p className="bg-red-500	w-4 h-4 rounded mr-2"></p>
          Todo
        </p>
      ),
      key: "TODO",
    },
    {
      label: (
        <p
          className="text-yellow-500 font-medium flex justify-flex-start items-center	"
          onClick={() => {
            handleChangeStatus(Status.PROCESSING);
          }}
        >
          <p className="bg-yellow-500	w-4 h-4 rounded mr-2"></p>
          Processing
        </p>
      ),
      key: "PROCESSING",
    },
    {
      label: (
        <p
          className="text-green-500 font-medium	flex justify-flex-start items-center"
          onClick={() => {
            handleChangeStatus(Status.DONE);
          }}
        >
          <p className="bg-green-500	w-4 h-4 rounded mr-2"></p>
          Done
        </p>
      ),
      key: "DONE",
    },
  ];
  const router = useRouter();
  const querys = router.query;
  const { data: session, status } = useSession();
  const [nameSearch, setNameSearch] = useState(querys.name);
  const [select, setselect] = useState(querys);
  const pathname = usePathname();

  useEffect(() => {
    if (router.isReady) {
      setDoeDate({
        doeDateStart: query.doeDateStart ? query.doeDateStart.toString() : "",
        doeDateEnd: query.doeDateend ? query.doeDateend.toString() : "",
      });
      let dataquery = query as unknown as DataFilters;
      setdataFilter(dataquery);
      setselect(querys);
      setNameSearch(querys.name);
    }
  }, [querys]);
  let query = router.query;

  const onChangePagination = async (page: number) => {
    setCurrent(page);
    router.push({
      query: { ...query, page: page },
    });
  };

  const [open, setOpen] = useState(false);
  const [valAssigne, setValAssigne] = useState("");
  const [taskID, settaskID] = useState("");
  const [datadetail, setdatadetail] = useState<IPageProps>();
  const [valMessageAssigned, setvalMessageAssigned] = useState("");

  const showDrawer = async (taskID: string, data: IPageProps) => {
    settaskID(taskID);
    setOpen(true);
    // dispatch(setloading(true));
    // let datadetails = await axiosConfig.get(`/api/task/${taskID}`);
    // dispatch(setloading(false));
    setdatadetail(data);
  };

  const onClose = () => {
    setOpen(false);
  };

  const handleChangeValAssigne = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setValAssigne(event.target.value);
  };
  const dispatch = useDispatch();

  const handleAssigned = async () => {
    dispatch(setloading(true));
    let checked = await axiosConfig.post(`/api/userontask`, {
      email: valAssigne,
      taskID: taskID,
      message: valMessageAssigned ? valMessageAssigned : "none!",
    });
    if (checked.data.message == "đã thêm trước đó") {
      dispatch(setloading(false));
      setValAssigne("");
      setvalMessageAssigned("");
      return message.warning(checked.data.message);
    }
    let data = await axiosConfig.get("/api/task", {
      params: { ...query, page: 1 },
    });
    dispatch(setTasks(data.data.datas));
    setdatadetail(
      data.data.datas.find((value: IPageProps) => value.id == taskID)
    );
    dispatch(setloading(false));
    message.success(checked.data.message);
    setValAssigne("");
    setvalMessageAssigned("");
  };

  const handleDisconnect = async (IDDisconnect: string) => {
    dispatch(setloading(true));
    await axiosConfig.patch(`/api/userontask`, {
      userID: IDDisconnect,
      taskID: taskID,
    });

    let data = await axiosConfig.get("/api/task", {
      params: { ...query, page: 1 },
    });
    console.log(data);
    dispatch(setTasks(data.data.datas));
    setdatadetail(
      data.data.datas.find((value: IPageProps) => value.id == taskID)
    );
    dispatch(setloading(false));
    message.success("Đã gỡ thành công");
  };
  type DataType = {
    user: {
      email: string;
      username: string;
      id: string;
    };
    message: string;
    assignedAt: string;
  };
  const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;

  const handleChangeMessage = (
    event: React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    setvalMessageAssigned(event.target.value);
  };

  const [dataFilter, setdataFilter] = useState<DataFilters>({
    status: null,
    point: null,
    priority: null,
    deadline: null,
    assigned: null,
    doeDateStart: null,
    doeDateend: null,
  });

  const optionsStatus: SelectProps["options"] = [];
  filterOptionStatus2.map((value) => {
    optionsStatus.push({
      label: value.lable,
      value: value.key,
    });
  });

  const optionsDeadline: SelectProps["options"] = [];
  filterOptionDeadline.map((value) => {
    optionsDeadline.push({
      label: value.lable,
      value: value.key,
    });
  });

  const optionsPoint: SelectProps["options"] = [];
  filterOptionPoint.map((value) => {
    optionsPoint.push({
      label: value.lable,
      value: value.key,
    });
  });

  const optionsPriority: SelectProps["options"] = [];
  filterOptionPriority.map((value) => {
    optionsPriority.push({
      label: value.lable,
      value: value.key,
    });
  });

  const handleChangeStatuss = (value: string[]) => {
    console.log(value);
    setdataFilter({ ...dataFilter, status: JSON.stringify(value) });
  };

  const handleChangeDeadline = (value: string) => {
    setdataFilter({ ...dataFilter, deadline: JSON.stringify(value) });
  };

  const handleChangePoint = (value: string) => {
    setdataFilter({ ...dataFilter, point: JSON.stringify(value) });
  };

  const handleChangePriority = (value: string) => {
    setdataFilter({ ...dataFilter, priority: JSON.stringify(value) });
  };

  const handleChangeAssinges = (value: string) => {
    if (value == "none") {
      setdataFilter({ ...dataFilter, assigned: null });
    } else {
      setdataFilter({ ...dataFilter, assigned: value });
    }
  };

  const handleGetAllOption = () => {
    setdataFilter({
      status: null,
      point: null,
      priority: null,
      deadline: null,
      assigned: null,
      doeDateStart: null,
      doeDateend: null,
    });
    router.push({
      pathname: pathname,
      query: {},
    });
  };

  const handleclose = () => {
    console.log({ ...dataFilter });
    router.push({
      // pathname: `/task/${session?.user?.name}`,
      query: { ...dataFilter, page: 1 },
    });
    setOpenFilter(false);
  };

  const onCloses = () => {
    setOpenFilter(false);
  };

  const { RangePicker } = DatePicker;
  type RangeValue = Parameters<
    NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>["onChange"]>
  >[0];

  const [doeDate, setDoeDate] = useState({ doeDateStart: "", doeDateEnd: "" });

  const handleDoeDateStart: DatePickerProps["onChange"] = (
    date,
    dateString
  ) => {
    setDoeDate({ ...doeDate, doeDateStart: dateString.toString() });
    setdataFilter({
      ...dataFilter,
      doeDateStart: dateString.toString(),
    });
  };

  const handleDoeDateEnd: DatePickerProps["onChange"] = (date, dateString) => {
    setDoeDate({ ...doeDate, doeDateEnd: dateString.toString() });
    setdataFilter({
      ...dataFilter,
      doeDateend: dateString.toString(),
    });
  };
  const dateFormats = "YYYY-MM-DD";
  return (
    <div
      className={` w-full  shadow-2xl border ${
        imageUrl ? " border-slate-500" : ""
      } `}
    >
      <Drawer
        className=""
        title="Assigned"
        placement="right"
        onClose={onClose}
        open={open}
      >
        <div className="flex ">
          <Input
            disabled={
              session?.user?.name !== datadetail?.author?.id ? true : false
            }
            value={valAssigne}
            onChange={handleChangeValAssigne}
            className="mr-2"
            placeholder="Email"
          />
          <Button
            disabled={
              session?.user?.name !== datadetail?.author?.id ? true : false
            }
            onClick={handleAssigned}
          >
            add
          </Button>
        </div>
        <div style={{ margin: "24px 0" }} />
        <TextArea
          showCount
          maxLength={20}
          disabled={
            session?.user?.name !== datadetail?.author?.id ? true : false
          }
          value={valMessageAssigned}
          onChange={handleChangeMessage}
          placeholder="Message......"
          autoSize={{ minRows: 2, maxRows: 6 }}
        />
        <div style={{ margin: "24px 0" }} />
        <div className="mt-4 relative">
          {loading ? (
            <div className="absolute w-full h-full bg-white top-0 left-0 z-50 opacity-50 flex items-center justify-center ">
              <Spin indicator={antIcon} />
            </div>
          ) : (
            ""
          )}
          <h1 className="flex items-center justify-start mt-2">
            <img
              className="w-10 h-10 rounded-full"
              src="https://cdn-icons-png.flaticon.com/256/4214/4214146.png"
              alt=""
            />
            <div className="text-xl ml-4 ">
              <p>{datadetail?.author?.email}</p>
              <p className="text-sm text-gray-500">
                {datadetail?.author?.username}
              </p>
            </div>
          </h1>
          <List
            itemLayout="horizontal"
            dataSource={datadetail ? datadetail.users : []}
            renderItem={(value: DataType, index) => (
              <List.Item className={`relative group/item !p-0 mt-4`}>
                <List.Item.Meta
                  className="group-hover/item:bg-slate-200 !p-4 overflow-auto max-h-full scrollbar-thin scrollbar-thumb-gray-300 scrollbar-track-gray-100"
                  avatar={
                    <Avatar
                      src={`https://xsgames.co/randomusers/avatar.php?g=pixel&key=${index}`}
                    />
                  }
                  title={
                    <div className="flex items-center ">
                      <div className="flex flex-col">
                        <p>{value.user.email}</p>
                        <p>{value.user.username}</p>
                      </div>
                      {session?.user?.name == datadetail?.author?.id ||
                      session?.user?.name == value.user.id ? (
                        <div
                          onClick={() => {
                            handleDisconnect(value.user.id);
                          }}
                          className="  group-hover/item:opacity-100 text-orange-800  absolute bottom-50 right-2 hover:cursor-pointer hover:text-orange-800 text-xl "
                        >
                          {session?.user?.name !== value.user.id ? (
                            <Button type="primary" danger>
                              Xóa
                            </Button>
                          ) : (
                            <Button type="primary" danger>
                              Rời
                            </Button>
                          )}
                        </div>
                      ) : (
                        ""
                      )}
                      <p className="absolute bottom-3 text-xs text-gray-500 right-0">
                        {new Date(value.assignedAt).toLocaleTimeString() +
                          "  " +
                          new Date(value.assignedAt).toLocaleDateString()}
                      </p>
                    </div>
                  }
                  description={
                    session?.user?.name == datadetail?.author?.id ||
                    session?.user?.name == value.user.id
                      ? value.message
                      : ""
                  }
                />
              </List.Item>
            )}
          />
        </div>
      </Drawer>
      {dataTasks?.length > 0 ? (
        dataTasks.slice(0, 5).map((value: IPageProps, index: number) => {
          let status = value.status.toString();
          return (
            <p
              key={index}
              className={`p-4 bg-white ${
                open && value.id == taskID
                  ? "border-2 border-indigo-500 shadow-md shadow-indigo-500/50 z-50"
                  : ""
              }  hover:bg-slate-100  group/item  w-full h-16 border flex justify-flext-start items-center relative ${
                status == Status.DONE
                  ? " !bg-green-50"
                  : status == Status.PROCESSING
                  ? " !bg-yellow-50"
                  : "!bg-red-50"
              }  ${imageUrl ? "background border-slate-500" : ""}`}
            >
              <Dropdown
                menu={{ items }}
                trigger={["click"]}
                placement="bottomRight"
                arrow={{ pointAtCenter: true }}
              >
                <div>
                  <div
                    onClick={() => {
                      setIdChange(value.id.toString());
                    }}
                    className={` w-8 h-8 border rounded-md  border-black cursor-pointer  flex items-center justify-center ${
                      status == Status.DONE
                        ? " bg-green-500"
                        : status == Status.PROCESSING
                        ? " bg-yellow-400"
                        : "bg-red-400"
                    }`}
                  >
                    {status == Status.DONE ? (
                      <div>
                        <div className="text-2xl text-white">
                          <AiOutlineCheck></AiOutlineCheck>
                        </div>
                      </div>
                    ) : status == Status.PROCESSING ? (
                      <div>
                        <div className="text-2xl text-white">
                          <AiOutlinePause></AiOutlinePause>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </Dropdown>

              <p
                className={` text-2xl ml-2 flex justify-between items-center w-full ${
                  value.status == Status.TODO
                    ? "text-red-500"
                    : value.status == Status.PROCESSING
                    ? " text-gray-700 text-yellow-500"
                    : "line-through decoration-stone-50 decoration-solid text-gray-700 text-green-500"
                }`}
              >
                <Link
                  href={{
                    pathname: `/detailTask/${value.id}`,
                  }}
                >
                  {value.name}
                </Link>
                <Button
                  type="primary"
                  className="absolute bottom-50 p-2 !bg-white border left-48 bg-white"
                  onClick={() => {
                    showDrawer(value.id, value);
                  }}
                >
                  <AiOutlineUserAdd className="text-black"></AiOutlineUserAdd>
                </Button>
                <p className="bg-white text-xl rounded-md absolute bottom-50 h-8 w-8 flex items-center justify-center border left-56 ">
                  {value?.users?.length}
                </p>
                {value.priority ? (
                  <p className="absolute buttom-50 left-80 translate-x-20 w-8 h-8 flex items-center justify-center">
                    <img
                      className="w-full"
                      src={
                        value.priority > 3
                          ? "https://cdn-icons-png.flaticon.com/512/2405/2405334.png"
                          : value.priority > 1
                          ? "https://cdn-icons-png.flaticon.com/256/5969/5969119.png"
                          : "https://cdn-icons-png.flaticon.com/512/5625/5625642.png"
                      }
                      alt=""
                    />
                    <span className="px-2 text-xl bg-white shadow-lg shadow-black-500/40 rounded-full">
                      {value.priority}
                    </span>
                  </p>
                ) : (
                  ""
                )}
                {value.point ? (
                  <p className="absolute buttom-50 left-96 translate-x-32 w-8 h-8 flex items-center justify-center">
                    <img
                      className="w-full"
                      src={
                        value.point > 7
                          ? "https://cdn-icons-png.flaticon.com/512/4149/4149877.png"
                          : value.point > 4
                          ? "https://cdn-icons-png.flaticon.com/512/7505/7505874.png"
                          : "https://cdn-icons-png.flaticon.com/512/900/900460.png"
                      }
                      alt=""
                    />
                    <span className="px-2 text-xl flex  bg-white shadow-lg shadow-black-500/40 rounded-full">
                      {" "}
                      {value.point}
                      <span className="text-xs">đ</span>
                    </span>
                  </p>
                ) : (
                  ""
                )}
                {session?.user?.name == value.author?.id ? (
                  <span className="text-sm mr-4 border p-1 bg-white rounded-md">
                    <button
                      onClick={() => {
                        showModal(value);
                      }}
                    >
                      edit
                    </button>
                  </span>
                ) : (
                  ""
                )}
                {createLoading && value.id == "" ? (
                  <div className="absolute top-50 right-12">
                    <Spin />
                  </div>
                ) : (
                  ""
                )}

                {value.deadStart &&
                value.deadEnd &&
                value.deadStart?.toString().split("T")[0] !==
                  value.deadEnd?.toString().split("T")[0] ? (
                  <span
                    className={`absolute bottom-50 right-20 text-sm  text-black flex justify-center items-center ${
                      imageUrl ? "text-white" : ""
                    }`}
                  >
                    {dayjs(value.deadStart).format("(HH:mm), DD/MM/YYYY")}
                    <span className="mb-2 mr-2 ml-2">
                      <SwapRightOutlined />
                    </span>
                    {dayjs(value.deadEnd).format("(HH:mm), DD/MM/YYYY")}
                  </span>
                ) : (
                  <span className="absolute bottom-50 right-20 mr-2 text-black text-sm">
                    Chưa có Deadline
                  </span>
                )}

                <span className="absolute bottom-50 -left-28 text-sm text-black shadow-xl bg-white w-22 px-3 py-1 rounded background">
                  {new Date(value.deadStart ? value.deadStart : "") <=
                    dateNow &&
                  new Date(value.deadEnd ? value.deadEnd : "") >= dateNow ? (
                    <span className="text-yellow-500">Đang diễn ra</span>
                  ) : value.deadStart == value.deadEnd ? (
                    ""
                  ) : new Date(value.deadStart ? value.deadStart : "") <
                    dateNow ? (
                    <span className="text-red-500">Đã hết hạn</span>
                  ) : (
                    <span className="text-green-500">Sắp tới</span>
                  )}
                </span>
              </p>

              {session?.user?.name == value.author?.id ? (
                <div
                  onClick={() => {
                    handleDelete(value.id);
                  }}
                  className=" opacity-0 group-hover/item:opacity-100 text-orange-800  absolute bottom-50 right-2 hover:cursor-pointer hover:text-orange-800 text-xl "
                >
                  <AiOutlineClose></AiOutlineClose>
                </div>
              ) : (
                ""
              )}
            </p>
          );
        })
      ) : (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
      )}
      {deleLoading ? (
        <p className="p-4 bg-white flex items-center justify-center">
          ...loading
        </p>
      ) : (
        ""
      )}
      {dataTasks?.length > 0 ? (
        <div className="text-center p-3 border-gray-500  group/item  w-full h-16 border-b flex justify-center items-center relative  ">
          <Pagination
            className=" bg-white opacity-80  rounded-md p-2"
            showSizeChanger={false}
            showTotal={(total) => `Total ${total / 2} items`}
            current={current}
            onChange={onChangePagination}
            total={amount * 2}
          />
        </div>
      ) : (
        ""
      )}
      <Drawer
        title="Filters"
        placement="right"
        onClose={onCloses}
        open={openFilter}
      >
        <div className="w-11/12 pb-4  ">
          <h1 className="mb-2">Due date:</h1>
          <div className="flex">
            <DatePicker
              className="mr-2"
              placeholder="date start"
              value={
                doeDate.doeDateStart
                  ? dayjs(doeDate.doeDateStart, dateFormats)
                  : null
              }
              onChange={handleDoeDateStart}
            />
            <DatePicker
              placeholder="date end"
              value={
                doeDate.doeDateEnd
                  ? dayjs(doeDate.doeDateEnd, dateFormats)
                  : null
              }
              onChange={handleDoeDateEnd}
            />
          </div>
        </div>
        <div className="w-11/12 pb-4">
          <h1 className="mb-2">Status:</h1>
          <Select
            mode="multiple"
            allowClear
            style={{ width: "100%" }}
            placeholder="status"
            value={
              dataFilter.status ? JSON.parse(dataFilter.status.toString()) : []
            }
            onChange={handleChangeStatuss}
            options={optionsStatus}
          />
        </div>
        <div className="w-11/12 pb-4">
          <h1 className="mb-2">Deadline:</h1>
          <Select
            mode="multiple"
            allowClear
            style={{ width: "100%" }}
            placeholder="Deadline"
            value={
              dataFilter.deadline
                ? JSON.parse(dataFilter.deadline.toString())
                : []
            }
            onChange={handleChangeDeadline}
            options={optionsDeadline}
          />
        </div>
        <div className="w-11/12 pb-4">
          <h1 className="mb-2">Point:</h1>
          <Select
            mode="multiple"
            allowClear
            style={{ width: "100%" }}
            placeholder="Point"
            value={
              dataFilter.point ? JSON.parse(dataFilter.point.toString()) : []
            }
            onChange={handleChangePoint}
            options={optionsPoint}
          />
        </div>
        <div className="w-11/12 pb-4">
          <h1 className="mb-2">Priority:</h1>
          <Select
            mode="multiple"
            allowClear
            style={{ width: "100%" }}
            placeholder="Priority"
            value={
              dataFilter.priority
                ? JSON.parse(dataFilter.priority.toString())
                : []
            }
            onChange={handleChangePriority}
            options={optionsPriority}
          />
        </div>
        <div className="pb-4">
          <h1 className="mb-2">Assinged:</h1>
          <Select
            value={
              dataFilter.assigned ? dataFilter.assigned.toString() : "none"
            }
            style={{ width: 120 }}
            onChange={handleChangeAssinges}
            options={[
              { label: "none", value: "none" },
              { label: "My tasks", value: "mytask" },
              { label: "Assigness", value: "assignes" },
            ]}
          />
        </div>
        <div className="mt-4">
          <Button
            className="w-28 inline-flex	 justify-center text-black bg-white hover:!border-black hover:!text-white hover:bg-black"
            onClick={handleGetAllOption}
          >
            GetAll
          </Button>
        </div>
        <div className="mt-16">
          <Button
            onClick={handleclose}
            className="py-6 text-xl flex justify-center items-center w-full border-2  border hover:bg-black hover:!text-white hover:!border-black"
          >
            TÌm kiếm
          </Button>
        </div>
      </Drawer>
    </div>
  );
};

export default ListTask;
