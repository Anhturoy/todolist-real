import { useEffect, useState } from "react";
import Dropzone from "dropzone";
import axiosConfig from "@/axiosConfig/axiosConfig";
import { useSession } from "next-auth/react";
import * as tus from "tus-js-client";
import { createClient } from "@supabase/supabase-js";
import { useDispatch } from "react-redux";
import {
  setImageList,
  setImageUrl,
  setUploadImgLoading,
} from "@/redux/counterSlice";

export default function UploadImage() {
  const { data: session, status } = useSession();

  // const projectId = "fpcrgzxbgqlthpwoturu";
  // const token = session?.user?.name;

  // function uploadFile(
  //   bucketName: string,
  //   fileName: string,
  //   file: File
  // ): Promise<void> {
  //   return new Promise((resolve, reject) => {
  //     const upload = new tus.Upload(file, {
  //       endpoint: `https://${projectId}.supabase.co/storage/v1/upload/resumable`,
  //       retryDelays: [0, 3000, 5000, 10000, 20000],
  //       headers: {
  //         authorization: `Bearer ${token}`,
  //         "x-upsert": "true", // optionally set upsert to true to overwrite existing files
  //       },
  //       uploadDataDuringCreation: true,
  //       metadata: {
  //         bucketName: bucketName,
  //         objectName: fileName,
  //         contentType: "image/png",
  //         // cacheControl: 3600,
  //       },
  //       chunkSize: 6 * 1024 * 1024, // NOTE: it must be set to 6MB (for now) do not change it
  //       onError: function (error: Error) {
  //         console.log("Failed because: " + error);
  //         reject(error);
  //       },
  //       onProgress: function (bytesUploaded: number, bytesTotal: number) {
  //         const percentage = ((bytesUploaded / bytesTotal) * 100).toFixed(2);
  //         console.log(`${bytesUploaded} ${bytesTotal} ${percentage}%`);
  //       },
  //       onSuccess: async function () {
  //         let data =await axiosConfig.post('/api/upload', { file: upload.file})
  //         console.log(`Download ${upload.file} from ${upload.url}`);
  //         resolve();
  //       },
  //     });

  //     // Check if there are any previous uploads to continue.
  //     return upload
  //       .findPreviousUploads()
  //       .then(function (previousUploads: any[]) {
  //         // Found previous uploads so we select the first one.
  //         if (previousUploads.length) {
  //           upload.resumeFromPreviousUpload(previousUploads[0]);
  //         }

  //         // Start the upload
  //         upload.start();
  //       });
  //   });
  // }
  const dispatch = useDispatch();
  const supabase = createClient(
    "https://skfxlwheyeyiexiymobd.supabase.co",
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InNrZnhsd2hleWV5aWV4aXltb2JkIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAzMTM2NjUsImV4cCI6MjAyNTg4OTY2NX0.ZiLKs2ik_WuuRf5Gv6ciFk-uG63ljEqMyy3uSVCiDUM"
    );
  const handleFileUpload = async (file: any) => {
    const { data, error } = await supabase.storage
      .from("ImageUpload")
      .upload(file.name, file);

    if (error) {
      if (error.message == "The resource already exists") {
        dispatch(setImageUrl(file.name));
        dispatch(setUploadImgLoading(true));
       await axiosConfig.post("/api/upload", {
          data: file.name,
        });
        const imageLists = await axiosConfig.get(
          `/api/user/${session?.user?.name}`
        );
        dispatch(setUploadImgLoading(false));
        dispatch(setImageList(imageLists.data.data[0].images));
        await axiosConfig.patch(`/api/user/${session?.user?.name}`, {
          background: file.name,
        });
      }
      console.error("Error uploading file:", error);
    } else {
      console.log("File uploaded successfully:", data);
      dispatch(setImageUrl(data.path));
      dispatch(setUploadImgLoading(true));
      await axiosConfig.post("/api/upload", { data: data.path });
      const imageLists = await axiosConfig.get(
        `/api/user/${session?.user?.name}`
      );
      dispatch(setUploadImgLoading(false));
      dispatch(setImageList(imageLists.data.data[0].images));
      await axiosConfig.patch(`/api/user/${session?.user?.name}`, {
        background: data.path,
      });
    }
  };

  useEffect(() => {
    const myDropzone = new Dropzone("#my-dropzone", {
      url: "/",
      maxFiles: 1, // Giới hạn chỉ được upload một file
      acceptedFiles: "image/*", // Chỉ chấp nhận các file có định dạng hình ảnh
      dictDefaultMessage: "+ Drop your file",
      maxFilesize: 5 * 1024 * 1024, // 5MB
    });
    myDropzone.on("addedfile", (file) => {
      console.log("File added:", file);
      // uploadFile("ImageUpload", file.name, file);
      handleFileUpload(file);
    });

    return () => {
      myDropzone.destroy();
    };
  }, []);

  return (
    <div>
      <div id="my-dropzone" className="dropzone"></div>
    </div>
  );
}
