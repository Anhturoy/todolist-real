import { useCallback, useEffect, useState } from "react";
import { Button, Modal, Tooltip, message } from "antd";
import RadioForm from "./RadioForm";
import ListTask from "./ListTask";
import { DatePicker, Space } from "antd";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import { useRouter } from "next/router";
import {
  LoadingOutlined,
  TableOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { Spin } from "antd";
import IntegerStep from "./Slider";
import { signOut, useSession } from "next-auth/react";
import { useSelector, useDispatch } from "react-redux";
import utc from "dayjs/plugin/utc";
import {
  setDeleLoading,
  setTasks,
  setTasksBackUp,
  setloading,
  setImageList,
  setImageUrl,
  setCreateLoading,
  setAlldataTasks,
} from "@/redux/counterSlice";
import { Avatar } from "antd";
import { AiOutlineCopy } from "react-icons/ai";
import axiosConfig from "@/axiosConfig/axiosConfig";
import { debounce } from "lodash";
import UploadImage from "./UploadImage";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";
import { Switch } from "@nextui-org/react";

const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;

type RangeValue = Parameters<
  NonNullable<React.ComponentProps<typeof DatePicker.RangePicker>["onChange"]>
>[0];

dayjs.extend(customParseFormat);

const { RangePicker } = DatePicker;

enum Status {
  TODO = "TODO",
  PROCESSING = "PROCESSING",
  DONE = "DONE",
}

enum DeadLine {
  OVER = "OVER",
  RUNNING = "RUNNING",
  COMING = "COMING",
}

export interface IPageProps {
  id: string;
  name: string;
  status: Status;
  deadStart?: string;
  deadEnd?: string;
  deadline?: DeadLine;
  priority: number;
  point: number;
  users: [];
}

export default function Home(props: IPageProps) {
  const dateNows = new Date(Date.now());
  const [inputVal, setInputVal] = useState("");
  const [dataEdit, setdataEdit] = useState<IPageProps>();

  const [idChange, setIdChange] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [amount, setAmount] = useState(1);
  const [current, setCurrent] = useState(1);
  const [user, setuser] = useState("");
  const [show, setShow] = useState(true);
  const router = useRouter();
  const query = router.query;
  const dispatch = useDispatch();
  const counter = useSelector(
    (state: {
      counter: {
        loading: boolean;
        dataEdit: IPageProps;
        dataTasks: IPageProps[];
        dataTasksBackUp: IPageProps[];
        imageUrl: string;
        createLoading: boolean;
        uploadImgLoading: boolean;
        imageList: { id: string; filename: string }[];
      };
    }) => state.counter
  );
  const { data: session, status } = useSession();
  const {
    loading,
    dataTasks,
    dataTasksBackUp,
    imageUrl,
    imageList,
    uploadImgLoading,
  } = counter;

  useEffect(() => {
    if (session && session?.user?.email) {
      setuser(JSON.parse(session.user.email).username);
    }
    if (status === "unauthenticated") {
      router.push("/auth/login");
    }

    if (router.isReady) {
        setCurrent(Number(query.page) ? Number(query.page) : 1);
        dispatch(setloading(true));
        axiosConfig.get(`/api/user/${session?.user?.name}`).then((value) => {
          dispatch(
            setImageUrl(
              value.data.data[0].background !== ""
                ? value.data.data[0].background
                : null
            )
          );
          dispatch(setImageList(value.data.data[0].images));
        });
        let pages = query.page ? query.page : 1;
        axiosConfig
          .get(`/api/task`, {
            params: { ...query, page: pages },
          })
          .then((value) => {
            setAmount(value.data.count);
            dispatch(setTasks(value.data.datas));
            dispatch(setTasksBackUp(value.data.datas));
            dispatch(setloading(false));
            dispatch(setAlldataTasks(value.data.alldata));
          })
          .catch((value) => {
            console.log(value);
          });
    }
  }, [router, status]);

  const handleLogout = async () => {
    await signOut({ callbackUrl: "/" });
  };

  // --------------------------------------them danh sach
  const keyDownHandler = async (event: React.KeyboardEvent<HTMLElement>) => {
    try {
      if (event.key == "Enter") {
        if (inputVal.length > 2) {
          let data = [
            {
              id: "",
              name: inputVal.charAt(0).toLocaleUpperCase() + inputVal.slice(1),
              status: Status.TODO,
              deadline: DeadLine.RUNNING,
            },
            ...dataTasks,
          ];
          dispatch(setTasks(data));
          dispatch(setCreateLoading(true));
          await axiosConfig.post("/api/task", {
            name: inputVal.charAt(0).toLocaleUpperCase() + inputVal.slice(1),
            status: Status.TODO,
            deadline: DeadLine.RUNNING,
          });
          setInputVal("");
          let newdata = await axiosConfig.get(`/api/task`, {
            params: { ...query },
          });
          setAmount(newdata.data.count);
          dispatch(setTasksBackUp(newdata.data.datas));
          dispatch(setTasks(newdata.data.datas));
          dispatch(setCreateLoading(false));
        } else {
          setInputVal("");
        }
      }
    } catch (error) {
      dispatch(setTasks([...dataTasksBackUp]));
      message.error("loi r ban oi");
    }
  };

  const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputVal(event.target.value);
  };

  // --------------------------------------Update status
  const [apiRunning, setapiRunning] = useState(false);
  useEffect(() => {
    const handleBeforeUnload = (event: BeforeUnloadEvent) => {
      // Kiểm tra xem API có đang chạy hay không
      if (apiRunning) {
        const confirmationMessage =
          "API đang chạy. Bạn có chắc chắn muốn làm mới trang?";
        event.preventDefault();
        event.returnValue = confirmationMessage;
      }
    };

    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, [apiRunning]);

  const [debouncedTasks, setDebouncedTasks] = useState<{
    [taskId: string]: ReturnType<typeof debounce>;
  }>({});

  const updateTaskStatus = async (taskId: string, statuss: Status) => {
    // Gọi API để cập nhật trạng thái của task
    try {
      await axiosConfig.patch(`/api/task/${idChange}`, {
        status: statuss,
      });
      dispatch(setTasksBackUp(dataTasks));
      setapiRunning(false);
    } catch (error) {
      dispatch(setTasks([...dataTasksBackUp]));
      message.error("loi r ban oi");
    }
  };

  const handleChangeStatus = async (statuss: Status) => {
    let data = [...dataTasks];
    let index = data.findIndex((value) => value.id == idChange);
    if (index !== -1) {
      data[index] = { ...data[index], status: statuss };
      setapiRunning(true);
      dispatch(setTasks(data));

      if (debouncedTasks[idChange]) {
        debouncedTasks[idChange].cancel();
      }
      const debounceFunction = debounce(
        () => updateTaskStatus(idChange, statuss),
        1000
      );
      debounceFunction();
      setDebouncedTasks((prevDebouncedTasks) => ({
        ...prevDebouncedTasks,
        [idChange]: debounceFunction,
      }));
    }
  };

  // --------------------------------------delete
  const handleDelete = async (indexs: string) => {
    try {
      let data = [...dataTasks];
      let index = data.findIndex((value) => value.id == indexs);
      if (index !== -1) {
        setapiRunning(true);
        data.splice(index, 1);
        dispatch(setTasks(data));
        dispatch(setDeleLoading(true));
        await axiosConfig.delete(`/api/task/${indexs}`);
        let newdata = await axiosConfig.get(`/api/task`, { params: query });
        setAmount(newdata.data.count);
        dispatch(setTasksBackUp(newdata.data.datas));
        dispatch(setTasks(newdata.data.datas));
        dispatch(setDeleLoading(false));
        setapiRunning(false);
      }
    } catch (error) {
      dispatch(setTasks([...dataTasksBackUp]));
      message.error("loi r ban oi");
    }
  };

  // -------------------------------------Thoong tin edit

  const handleChangleEdit = (event: React.ChangeEvent<HTMLInputElement>) => {
    let news = { ...dataEdit, name: event.target.value } as IPageProps;
    setdataEdit(news);
  };

  // --------------------------------------Modal
  const showModal = (value: IPageProps) => {
    setdataEdit(value);
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // --------------------------------------Update

  const handleOk = async () => {
    try {
      let dataNewChange = dataEdit as IPageProps;
      if (dataNewChange.name.length !== 0) {
        let data = [...dataTasks];
        let index = data.findIndex((value) => value.id == dataNewChange.id);
        if (index !== -10) {
          data[index] = { ...data[index], ...dataNewChange };
          dispatch(setTasks(data));
          setIsModalOpen(false);
          setapiRunning(true);
          await axiosConfig.put(`/api/task/${dataNewChange.id}`, dataNewChange);
          dispatch(setTasksBackUp(data));
          setapiRunning(false);
        }
      }
    } catch (error) {
      dispatch(setTasks([...dataTasksBackUp]));
      message.error("loi r ban oi");
    }
  };

  // --------------------------------------Update date

  const handleChangeDate = (a: RangeValue, b: string[]) => {
    dayjs.extend(utc);
    let start = b[0] == "" ? Date.now() : b[0];
    let end = b[1] == "" ? Date.now() : b[1];
    if (dataEdit) {
      let newdata = {
        ...dataEdit,
        deadStart: new Date(start).toISOString(),
        deadEnd: new Date(end).toISOString(),
        deadline:
          new Date(start) < dateNows && new Date(end) > dateNows
            ? DeadLine.RUNNING
            : new Date(start) > dateNows
            ? DeadLine.COMING
            : new Date(end) < dateNows
            ? DeadLine.OVER
            : undefined,
      };
      setdataEdit(newdata);
    }
  };

 const options = [
    { label: "Todo", value: Status.TODO },
    { label: "Processing", value: Status.PROCESSING },
    { label: "Done", value: Status.DONE },
  ];

  const dateFormat = "YYYY-MM-DD HH:mm:ss";
  const dateNow = new Date(Date.now()).toString();

  const [copied, setCopied] = useState(false);

  const handleCopy = () => {
    const textToCopy =  session && session?.user?.email ? JSON.parse(session.user.email).email : ""

    navigator.clipboard
      .writeText(textToCopy)
      .then(() => {
        message.success("Copied userID to clipboard");
        setCopied(true);
      })
      .catch((error) => {
        console.error("Lỗi khi sao chép:", error);
      });
  };

  const handleChangeBackground = (filename: string) => {
    dispatch(setImageUrl(filename));
    axiosConfig.patch(`/api/user/${session?.user?.name}`, {
      background: filename,
    });
  };

  const handleDeleteBg = async (imageID: string, imageName: string) => {
    try {
      if (imageName == imageUrl) {
        dispatch(setImageUrl(""));
        axiosConfig.patch(`/api/user/${session?.user?.name}`, {
          background: "",
        });
      }
      let newimagelist = [...imageList].filter((value) => value.id !== imageID);
      dispatch(setImageList(newimagelist));
      axiosConfig.delete(`/api/upload/${imageID}`);
    } catch (error) {}
  };

  const [isModalOpens, setIsModalOpen1] = useState(false);

  const showModal1 = () => {
    setIsModalOpen1(true);
  };

  const handleOk1 = () => {
    setIsModalOpen1(false);
  };

  const handleCancel1 = () => {
    setIsModalOpen1(false);
  };
  const [openFilter, setOpenFilter] = useState(false);

  const showDrawer = () => {
    setOpenFilter(true);
  };

  const [nameSearch, setNameSearch] = useState(query.name);

  const handleNameSearch = async (event: React.KeyboardEvent<HTMLElement>) => {
    if (event.key == "Enter") {
      router.push({
        query: { ...query, name: nameSearch },
      });
    }
  };

  const handleChangeNameSearch = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setNameSearch(event.target.value);
    if (event.target.value == "") {
      router.push({
        query: { ...query, name: null },
      });
    }
  };

  return (
    <main
      className={`relative flex min-h-screen flex-col items-center justify-flex-start p-0 mb-0 bg-cover !z-50   ${
        show ? "bg-white" : "bg-black"
      } mb-0`}
    >
      <Modal
        title="Background"
        open={isModalOpens}
        onOk={handleOk1}
        onCancel={handleCancel1}
      >
        <div className="w-full h-full flex flex-col items-center justify-center">
          <div className="w-10/12	 ">
            <UploadImage></UploadImage>
          </div>
          <div className="flex items-center justify-center w-full">
            <div className="flex  max-h-60 flex-wrap items-center w-10/12	  overflow-auto scrollbar-thin scrollbar-thumb-gray-300 scrollbar-track-gray-100">
              {uploadImgLoading ? (
                <div className="flex items-center justify-center w-32 h-20">
                  <Spin />
                </div>
              ) : (
                ""
              )}

              {imageList
                ? [...imageList].reverse().map((value) => {
                    return (
                      <p
                      key={value.id}
                        className={`rounded-md group/item overflow-hidden my-1 cursor-pointer  relative ${
                          value.filename == imageUrl
                            ? "border-2 border-amber-600"
                            : ""
                        }`}
                      >
                        <LazyLoadImage
                          effect="blur"
                          onClick={() => {
                            handleChangeBackground(value.filename);
                          }}
                          className="w-32 h-20 p-1"
                          src={`https://skfxlwheyeyiexiymobd.supabase.co/storage/v1/object/public/ImageUpload/${value.filename
                            ?.split(" ")
                            .join("%20")}`}
                          alt=""
                        />
                        <Button
                          onClick={() => {
                            handleDeleteBg(value.id, value.filename);
                          }}
                          className="absolute top-0 left-0 opacity-0 group-hover/item:opacity-100 "
                          danger
                          type="primary"
                        >
                          Xóa
                        </Button>
                      </p>
                    );
                  })
                : ""}
            </div>
          </div>
        </div>
      </Modal>
     {imageUrl ?  <img
        className="absolute bg-auto top-0 left-0 w-full h-full bg-repeat	 imgs"
        src={`https://skfxlwheyeyiexiymobd.supabase.co/storage/v1/object/public/ImageUpload/${imageUrl
          ?.split(" ")
          .join("%20")}`}
        alt=""
      /> : ''}
      {loading ? (
        <div className="absolute w-full h-full bg-white top-0 left-0 z-50 opacity-50 flex items-center justify-center ">
          <Spin indicator={antIcon} />
        </div>
      ) : (
        ""
      )}
      <Modal
        title="Edit"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <div className="items-center justify-center flex">
          <span>Task: </span>
          <input
            className="ml-2 underline-offset-8 w-full"
            type="text"
            onChange={handleChangleEdit}
            value={dataEdit?.name}
          />
        </div>
        <div className="items-flex-start justify-center flex mt-4">
          <span>Date:</span>
          <div className="w-full ml-2">
            <Space direction="vertical" size={12}>
              <RangePicker
                showTime
                onChange={handleChangeDate}
                value={[
                  dayjs(
                    `${
                      dataEdit?.deadStart == dateNow
                        ? dateNow
                        : dayjs(dataEdit?.deadStart)
                            .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
                            .toString()
                    }`,
                    dateFormat
                  ),
                  dayjs(
                    `${
                      dataEdit?.deadEnd == dateNow
                        ? dateNow
                        : dayjs(dataEdit?.deadEnd)
                            .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
                            .toString()
                    }`,
                    dateFormat
                  ),
                ]}
              />
            </Space>
          </div>
        </div>
        <div className="items-flex-start justify-center flex mt-4">
          <span>Status:</span>
          <div className="w-full ml-2">
            <RadioForm
              options={options}
              setdataEdit={setdataEdit}
              dataEdit={dataEdit}
            />
          </div>
        </div>
        <div className="items-center justify-flex-start flex mt-4">
          <span className="w-14">Priority:</span>
          <div className="w-full ml-4 ">
            <IntegerStep
              dataEdit={dataEdit}
              setdataEdit={setdataEdit}
              max={5}
              keys="priority"
            />
          </div>
        </div>
        <div className="items-center justify-flex-start flex mt-4">
          <span className="w-14">Point:</span>
          <div className="w-full ml-4  ">
            <IntegerStep
              dataEdit={dataEdit}
              setdataEdit={setdataEdit}
              max={10}
              keys="point"
            />
          </div>
        </div>
      </Modal>
      <h1 className="text-8xl  text-rose-200  mt-[50px] mb-9 "> </h1>
      <div className="w-9/12 h-full flex justify-between items-center  ">
        <h1 className="p-2 text-xl rounded border bg-green-500 w-fit text-white flex items-center">
          <Avatar
            style={{ backgroundColor: "#fde3cf", color: "#f56a00" }}
            className="mr-2 px-4"
          >
            {user.charAt(0).toLocaleUpperCase()}
          </Avatar>
          <div>{user}</div>
          <Switch
            className="m-2"
            size="sm"
            checked={show}
            onChange={() => setShow(!show)}
          />
          <div className="text-sm bg-white rounded-md text-gray-400 ml-4 p-1.5 shadow-2xl">
            {session && session?.user?.email ? JSON.parse(session.user.email).email : ""}
          </div>
          <Tooltip title={copied ? "copied" : "copy!!"}>
            <Button onClick={handleCopy} className="!bg-white p-2 ml-2">
              <AiOutlineCopy />
            </Button>
          </Tooltip>
        </h1>
        <div className="flex">
          <label className="relative block h-full">
            <span className="sr-only">Search</span>
            <span className="absolute inset-y-0 left-0 flex items-center pl-2">
              <SearchOutlined />
            </span>
            <input
              autoComplete="off"
              onKeyDown={handleNameSearch}
              onChange={handleChangeNameSearch}
              value={nameSearch}
              className="placeholder:italic  placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-3 pl-9 pr-3  shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Search for anything..."
              type="text"
              name="search"
            />
          </label>
          <button
            onClick={() => {
              router.push(`/calender`);
            }}
            className="p-2 ml-1 rounded border text-gray-800 hover:!border-black hover:bg-black hover:text-white  hover:opacity-80  flex items-center justify-center hover:text-white"
          >
            <TableOutlined className="text-white" />{" "}
            <span className="ml-2">Xem dạng bảng</span>
          </button>
          <button
            className="p-2 ml-1 rounded text-gray-800 hover:!border-black hover:bg-black hover:text-white  border hover:opacity-80 "
            onClick={showModal1}
          >
            Background
          </button>
          <button
            className=" mx-1 flex items-center justify-center p-2 rounded text-black border-black   border hover:opacity-80 bg-gray-100 "
            onClick={showDrawer}
          >
            <img
              className="w-4 h-4  mr-1  -rotate-90 "
              src="https://cdn.icon-icons.com/icons2/1673/PNG/512/options2outline_110906.png"
              alt=""
            />
            Filters
          </button>
          <button
            className="p-2 rounded border bg-red-500 text-white hover:bg-red-800  hover:text-white "
            onClick={handleLogout}
          >
            Logout
          </button>
        </div>
      </div>
      <div className="flex flex-col items-start justify-flex-start w-9/12">
        <div className="ml-4 flex flex-start text-xs mt-2	flex-wrap">
          {Object.keys(query ?? {}).map((key) => {
            if (
              key !== "homepage" &&
              key !== "homeID" &&
              key !== "currentDates" &&
              key !== "currentMonth" &&
              key !== "firstCellDate" &&
              key !== "lastCellDate" &&
              key !== "mode" &&
              key !== "page" &&
              key !== "currentYear"
            ) {
              if (
                key == "assigned" ||
                key == "name" ||
                key == "mode" ||
                key == "currentYear" ||
                key == "doeDateStart" ||
                key == "doeDateend"
              ) {
                return (
                  <p key={key} className="mr-1 pb-2 text-white">
                    {query[key] ? (
                      <>
                        <span>
                          {key == "doeDateStart"
                            ? "From"
                            : key == "doeDateend"
                            ? "To"
                            : key}
                          :
                        </span>{" "}
                        <span className="border-b   p-0.5">
                          {key == "doeDateStart" || key == "doeDateend"
                            ? dayjs(query[key]?.toString()).format(
                                "DD-MM-YYYY"
                              )
                            : query[key]}
                        </span>{" "}
                        /
                      </>
                    ) : (
                      ""
                    )}
                  </p>
                );
              }
              if (key == "point" || key == "priority") {
                return (
                  <p key={key} className="mr-1 pb-2 text-white">
                    {JSON.parse(query[`${key}`]?.toString() ?? "").length >
                    0 ? (
                      <>
                        <span>{key}:</span>{" "}
                        <span className="border-b   p-0.5">
                          {JSON.parse(query[`${key}`]?.toString() ?? "").join(
                            " _ "
                          )}
                        </span>{" "}
                        /
                      </>
                    ) : (
                      ""
                    )}
                  </p>
                );
              }
              return (
                <p key={key} className="mr-1 pb-2 text-white">
                  {JSON.parse(query[`${key}`]?.toString() ?? "").length >
                  0 ? (
                    <>
                      <span>{key}:</span>{" "}
                      <span className="border-b   p-0.5">
                        {JSON.parse(query[`${key}`]?.toString() ?? "").join(
                          " - "
                        )}
                      </span>{" "}
                      /
                    </>
                  ) : (
                    ""
                  )}
                </p>
              );
            }
          })}
        </div>
        <input
        autoComplete="off"
          onKeyDown={keyDownHandler}
          value={inputVal}
          onChange={handleChangeInput}
          className={` p-4  text-2xl placeholder:italic placeholder:opacity-30 placeholder:text-slate-400 ${
            imageUrl ? "placeholder:opacity-100" : ""
          } background ${
            show ? "bg-white " : "bg-black !text-white"
          } w-full shadow-md`}
          placeholder="What needs to be done ? "
        ></input>

        <ListTask
          openFilter={openFilter}
          setOpenFilter={setOpenFilter}
          setIdChange={setIdChange}
          handleChangeStatus={handleChangeStatus}
          showModal={showModal}
          handleDelete={handleDelete}
          amount={amount}
          setAmount={setAmount}
          setCurrent={setCurrent}
          current={current}
        />
      </div>
    </main>
  );
}
