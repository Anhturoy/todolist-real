import { useRouter } from 'next/router';
import * as React from 'react';
import Home from '../containers/Home';
import Calenders from '../containers/Calender';
import NotFoundPage from '../404';
export interface IAppProps {
}

enum Status {
    TODO = "TODO",
    PROCESSING = "PROCESSING",
    DONE = "DONE",
  }

export default function App (props: IAppProps) {
    const router = useRouter();
    const query = router.query;
    if(query.homepage == 'homepage'){
        return <Home id={''} name={''} status={Status.TODO} priority={0} point={0} users={[]}></Home>
    }
    if(query.homepage == 'calender'){
        return <Calenders id={''}></Calenders>
    }else{
        return <NotFoundPage></NotFoundPage>
    }
    
}
