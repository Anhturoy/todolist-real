import React, { useEffect, useState } from "react";
import { Collapse } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { setUsers, setloading } from "@/redux/counterSlice";
import { IPageProps } from "..";
import { SwapRightOutlined } from "@ant-design/icons";
import { signOut } from "next-auth/react";
import { Spin , Empty} from "antd";
import axiosInstance from "@/axiosConfig/axiosConfig";


interface DashBoardProps {}
enum Status {
  TODO = "TODO",
  PROCESSING = "PROCESSING",
  DONE = "DONE",
}

const Dashboard: React.FC<DashBoardProps> = ({}) => {
  const dispatch = useDispatch();
  const counter = useSelector(
    (state: {
      counter: {
        loading: boolean;
        dataEdit: IPageProps;
        dataUsers: { id: string; email: string, role: string }[];
      };
    }) => state.counter
  );
  const { dataUsers, loading } = counter;
  const [dataTask, setdataTask] = useState<IPageProps[]>();

  useEffect(() => {
    dispatch(setloading(true));
    axiosInstance
      .get("/api/admin")
      .then((value) => {
        dispatch(setloading(false));
        console.log(value.data.data);
        dispatch(setUsers(value.data.data));
      })
      .catch((value) => {
        console.log(value);
      });
  }, []);

  const onChange = async (key: string | string[]) => {
    let dataTasks = await axiosInstance.get(`/api/admin/${key}`)
    console.log(dataTasks.data.data);
    setdataTask(dataTasks.data.data);
  };
  const handleLogout = async () => {
    await signOut({ callbackUrl: "/" });
  };
  const { Panel } = Collapse;
  const dateNow = new Date(Date.now());

  return (
    <div className="w-screen h-screen flex bg-gray-200 items-center justify-center">
      <div className="w-9/12 h-3/4 bg-white rounded-md p-4">
        <div className="flex items-center justify-between">
          <h1 className="p-4">Dashboard</h1>
          <button
            className="p-2 rounded border bg-gray-500 text-white hover:bg-gray-100  hover:text-red-500 hover:font-bold"
            onClick={handleLogout}
          >
            Logout
          </button>
        </div>
        <div className={`overflow-auto scrollbar-thin scrollbar-thumb-gray-400 scrollbar-track-gray-300 h-5/6 ${loading ? 'flex items-center justify-center' : ''}`}>
            {loading ? <Spin></Spin> : ''}
          <Collapse accordion onChange={onChange}>
            {dataUsers?.map((value) => {
              if( value.role !== "ADMIN") { 
                return (
                  <Panel header={value.email} key={value.id}>
                    <div className="overflow-auto scrollbar-thin scrollbar-thumb-gray-400 scrollbar-track-gray-300 max-h-60">
                      { dataTask?.length !== undefined ? dataTask.length == 0 ? <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />: (
                        dataTask?.map((value, index) => {
                          return (
                            <div
                            key={index}
                              className={`p-4 bg-white  w-full h-16 border flex justify-flext-start items-center   ${
                                value.status == Status.DONE
                                  ? " !bg-green-50"
                                  : value.status == Status.PROCESSING
                                  ? " !bg-yellow-50"
                                  : "!bg-red-50"
                              }`}
                            >
                              <div
                                className={` w-8 h-8 border rounded-md  border-black cursor-pointer  flex items-center justify-center mr-20 ${
                                  value.status == Status.DONE
                                    ? " bg-green-500"
                                    : value.status == Status.PROCESSING
                                    ? " bg-yellow-400"
                                    : "bg-red-400"
                                }`}
                              ></div>
                              <p
                                className={` text-2xl ml-2 flex justify-between items-center w-full ${
                                  value.status == Status.TODO
                                    ? "text-red-500"
                                    : value.status == Status.PROCESSING
                                    ? " text-gray-700 text-yellow-500"
                                    : "line-through decoration-stone-50 decoration-solid text-gray-700 text-green-500"
                                }`}
                              >
                                {value.name}
                                {value.priority ? (
                                  <div className=" buttom-50 left-80 translate-x-20 w-8 h-8 flex items-center justify-center">
                                    <img
                                      className="w-full"
                                      src={
                                        value.priority > 3
                                          ? "https://cdn-icons-png.flaticon.com/512/2405/2405334.png"
                                          : value.priority > 1
                                          ? "https://cdn-icons-png.flaticon.com/256/5969/5969119.png"
                                          : "https://cdn-icons-png.flaticon.com/512/5625/5625642.png"
                                      }
                                      alt=""
                                    />
                                    <span className="px-2 text-xl bg-white shadow-lg shadow-black-500/40 rounded-full">
                                      {value.priority}
                                    </span>
                                  </div>
                                ) : (
                                  ""
                                )}
                                {value.point ? (
                                  <div className=" buttom-50 left-96 translate-x-32 w-8 h-8 flex items-center justify-center">
                                    <img
                                      className="w-full"
                                      src={
                                        value.point > 7
                                          ? "https://cdn-icons-png.flaticon.com/512/4149/4149877.png"
                                          : value.point > 4
                                          ? "https://cdn-icons-png.flaticon.com/512/7505/7505874.png"
                                          : "https://cdn-icons-png.flaticon.com/512/900/900460.png"
                                      }
                                      alt=""
                                    />
                                    <span className="px-2 text-xl flex  bg-white shadow-lg shadow-black-500/40 rounded-full">
                                      {value.point}
                                      <span className="text-xs">đ</span>
                                    </span>
                                  </div>
                                ) : (
                                  ""
                                )}
  
                                {value.deadStart && value.deadEnd ? (
                                  <span className=" bottom-50 right-20 text-sm  text-black flex justify-center items-center">
                                    {new Date(
                                      value.deadStart
                                    ).toLocaleDateString()}
                                    <span className="mb-2 mr-2 ml-2">
                                      <SwapRightOutlined />
                                    </span>
                                    {new Date(value.deadEnd).toLocaleDateString()}
                                  </span>
                                ) : (
                                  <span className=" bottom-50 right-20 mr-2 text-black text-sm">
                                    Chưa có Deadline
                                  </span>
                                )}
  
                                <span className=" bottom-50 left-24 text-sm text-black shadow-xl bg-white w-22 px-3 py-1 rounded  ">
                                  {new Date(
                                    value.deadStart ? value.deadStart : ""
                                  ) <= dateNow &&
                                  new Date(value.deadEnd ? value.deadEnd : "") >=
                                    dateNow ? (
                                    <span className="text-yellow-500">
                                      Đang diễn ra
                                    </span>
                                  ) : new Date(
                                      value.deadStart ? value.deadStart : ""
                                    ) < dateNow ? (
                                    <span className="text-red-500">
                                      Đã hết hạn
                                    </span>
                                  ) : !value.deadStart && !value.deadEnd ? (
                                    ""
                                  ) : (
                                    <span className="text-green-500">
                                      Sắp tới
                                    </span>
                                  )}
                                </span>
                              </p>
                            </div>
                          );
                        })
                      ) : (
                        <Spin></Spin>
                      )}
                    </div>
                  </Panel>
                );
              }
            })}
          </Collapse>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
