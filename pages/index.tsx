import { useRouter } from "next/router";
import { signIn, useSession } from "next-auth/react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setloading } from "@/redux/counterSlice";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

enum Status {
  TODO = "TODO",
  PROCESSING = "PROCESSING",
  DONE = "DONE",
}

enum DeadLine {
  OVER = "OVER",
  RUNNING = "RUNNING",
  COMING = "COMING",
}

export interface IPageProps {
  users: [];
  id: string;
  name: string;
  status: Status;
  deadStart?: string;
  deadEnd?: string;
  deadline?: DeadLine;
  priority: number;
  point: number;
  author?: {
    email: string;
    username: string;
    id: string;
  };
  task?: [];
}

const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;

interface AppProps {}

export default function App(props: AppProps) {
  const counter = useSelector((state: any) => state.counter);
  const loading = counter.loading;

  const router = useRouter();
  const { status, data } = useSession();
  const dispatch = useDispatch();

  useEffect(() => {
    if (status == "loading") {
      dispatch(setloading(true));
    } else {
      dispatch(setloading(false));
    }
    if (status === "authenticated") {
      router.push(`/homepage`);
    }
  }, [status]);
  return (
    <div className="relative w-screen h-screen bg-black flex items-center justify-center">
      {loading ? (
        <div className="absolute w-full h-full bg-white top-0 left-0 z-50 opacity-50 flex items-center justify-center ">
          <Spin indicator={antIcon} />
        </div>
      ) : (
        ""
      )}
      <img
        src="https://cdn-icons-png.flaticon.com/512/3884/3884864.png"
        alt=""
        className=" w-90 h-90 "
      />
      <div className="flex flex-col py-14 px-4 rounded-full border bg-black">
        <button
          className="p-10 rounded-full m-2 border bg-blue-500 hover:bg-white text-xl"
          onClick={() => {
            signIn();
          }}
        >
          Login
        </button>
        <button
          className="p-10 rounded-full m-2 border bg-green-500 hover:bg-white text-xl "
          onClick={() => {
            router.push("/auth/register");
          }}
        >
          Register
        </button>
      </div>
    </div>
  );
}
