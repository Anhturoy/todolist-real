import React, { useEffect }  from "react";
import { Button, Form, Input , Spin, notification } from "antd";
import { signIn, useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { setloading } from "@/redux/counterSlice";
import { IPageProps } from "..";
import { LoadingOutlined } from "@ant-design/icons";
import axiosConfig from "@/axiosConfig/axiosConfig";



interface RegisterProps {}

const RegisterPage: React.FC<RegisterProps> = ({}) => {
  const router = useRouter()
  const { status, data } = useSession();
  const dispatch = useDispatch()
  const counter = useSelector((state: {counter: {loading:boolean , dataEdit: IPageProps}}) => state.counter);
  const {loading} = counter
  const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;

  useEffect(() => {
    if (status === "authenticated") {
      router.push(`/homepage`);
    }
  }, [status]);


  function notifications (data: string){
    notification.open({
      message: `${data}`,
      onClick: () => {
        console.log('Notification Clicked!');
      },
    });
  }

  const onFinish =async (values: any) => {
    if(validatePassword(values.password)){
      if(validateUserName(values.email)){
        dispatch(setloading(true))
        let data = await axiosConfig.post('/api/auth/register', {
          username: values.username,
          password: values.password,
          email: values.email,
          background: ''
        })
        dispatch(setloading(false))
        if(!data.data.message){
          notifications('Register success')
          router.push('/auth/login')
        }else{
          notifications(data.data.message)
        }
      }else{ notifications('Tên quá ngắn') }
    }else{
      notifications('Mật khẩu sai định dạng')
    }
  };

  function validatePassword(pw: string) {
    return (
      /[A-Z]/.test(pw) &&
      /[a-z]/.test(pw) &&
      /[0-9]/.test(pw) &&
      /[^A-Za-z0-9]/.test(pw) &&
      pw.length > 4
    );
  }

  function validateUserName(pw: string) {
    return (
      /[a-z]/.test(pw) &&
      pw.length > 2
    );
  }

  
  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  
  return (
    <div className="w-screen h-screen flex bg-gray-200 items-center justify-center relative ">
       {loading ? <div className="absolute w-full h-full bg-white top-0 left-0 z-50 opacity-50 flex items-center justify-center ">
          <Spin indicator={antIcon} />
        </div>: '' }
      <div className="w-2/4 h-2/4 bg-white rounded-lg shadow-2xl p-10 grid grid-cols-2 gap-4">
      <div className=" w-full h-full">
          <img src="https://cdn-icons-png.flaticon.com/512/5807/5807739.png" className="w-full h-full" alt="" />
        </div>
        <div className="w-full h-full">
          <h1 className="text-2xl font-bold">Register</h1>
          <div className="mt-4">
            <Form
              name="basic"
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                // label="Password"
                name="email"
                rules={[
                  {
                    required: true,
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                ]}
              >
                <Input placeholder="Your Email..."/>
              </Form.Item>

              <Form.Item
                name="username"
                rules={[
                  { required: true, message: "Please input your username!" },
                ]}
              >
                <Input placeholder="User Name..."/>
              </Form.Item>

              <Form.Item
                name="password"
                rules={[
                  { required: true, message: "Please input your password!" },
                ]}
              >
                <Input.Password placeholder="Password..."/>
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="bg-blue-500"
                >
                  Reigster
                </Button>
              </Form.Item>
            </Form>
            <p className="text-sm text-center">Đã có tài khoản ?<span onClick={()=>{signIn()}} className="text-blue-500 underline cursor-pointer decoration-solid">Login</span></p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegisterPage;
