import React, { useEffect } from "react";
import { Button, Form, Input, Spin, notification } from "antd";
import Link from "next/link";
import { signIn, useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { setloading } from "@/redux/counterSlice";
import { IPageProps } from "..";
import { LoadingOutlined } from "@ant-design/icons";

interface loginProps {}

const LoginPage: React.FC<loginProps> = ({}) => {
  const router = useRouter();
  const { status, data } = useSession();
  const dispatch = useDispatch();
  const counter = useSelector(
    (state: { counter: { loading: boolean; dataEdit: IPageProps } }) =>
      state.counter
  );
  const { loading } = counter;

  function notifications(data: string) {
    notification.open({
      message: `${data}`,
      onClick: () => {
        console.log("Notification Clicked!");
      },
    });
  }
  const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;

  useEffect(() => {
    if (status === "authenticated") {
      router.push(`/homepage`);
    }
  }, [status]);

  const onFinish = async (values: any) => {
    if (validatePassword(values.password)) {
      dispatch(setloading(true));
      const res = await signIn("credentials", {
        email: values.email,
        password: values.password,
        redirect: false,
      });
      dispatch(setloading(false));
      if (res?.ok) {
        notifications("Login success");
      } else {
        notifications(res?.error ? res?.error : "error");
      }
    } else {
      notifications("mật khẩu sai định dạng");
    }
  };

  function validatePassword(pw: string) {
    return (
      /[A-Z]/.test(pw) &&
      /[a-z]/.test(pw) &&
      /[0-9]/.test(pw) &&
      /[^A-Za-z0-9]/.test(pw) &&
      pw.length > 4
    );
  }

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen flex bg-gray-200 items-center justify-center relative">
      {loading ? (
        <div className="absolute w-full h-full bg-white top-0 left-0 z-50 opacity-50 flex items-center justify-center ">
          <Spin indicator={antIcon} />
        </div>
      ) : (
        ""
      )}
      <div className="w-2/4 h-2/4 bg-white rounded-lg shadow-2xl p-4 grid grid-cols-2 gap-4 relative">
        <div className=" w-full h-full">
          <img
            src="https://cdn-icons-png.flaticon.com/512/4661/4661409.png"
            className="w-full h-full"
            alt=""
          />
        </div>
        <div className="h-full w-0.5 bg-gray-500 absolute top-0 inset-1/2"></div>
        <div className="w-full h-full flex flex-col items-center justify-evenly">
          <h1 className="text-2xl font-bold ">Login</h1>
          <div className="mb-4">
            <Form
              name="basic"
              className="w-full"
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                name="email"
                rules={[
                  {
                    required: true,
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                ]}
              >
                <Input placeholder="Your Email..." />
              </Form.Item>

              <Form.Item
                name="password"
                rules={[
                  { required: true, message: "Please input your password!" },
                ]}
              >
                <Input.Password placeholder="Password..." />
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="bg-blue-500"
                >
                  Login
                </Button>
              </Form.Item>
            </Form>
            <p className="text-sm text-center">
              Chưa có tài khoản ?
              <Link href={{ pathname: "/auth/register" }}>
                <span className="text-blue-500 underline  decoration-solid">
                  Register
                </span>
              </Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
