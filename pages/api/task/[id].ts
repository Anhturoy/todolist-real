import { PrismaClient } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";

const prisma = new PrismaClient();

async function handler(req: NextApiRequest, res: NextApiResponse) {
  const token = await getToken({ req, secret: process.env.JWT_SECRET });

  try {
    const { method, query } = req;

    switch (method) {
      case "GET":
        const dataDetail = await prisma.tasklist.findMany({
          where: {
            id: query.id?.toString(),
            OR: [
              {
                authorID: token?.sub,
              },
              {
                users: {
                  some: {
                    user: {
                      id: token?.sub,
                    },
                  },
                },
              },
            ],
          },
          include: {
            users: {
              include: { user: true },
            },
            author: true,
          },
        });
        res.status(200).json({ dataDetail });
        break;

      case "DELETE":
        await prisma.usersOnTask.deleteMany({
          where: {
            taskID: query.id ? query.id.toString() : "",
          },
        });
        const deletes = await prisma.tasklist.delete({
          where: {
            id: query.id?.toString(),
          },
        });
        res.status(200).send({ deletes });
        break;

      case "PUT":
        const update = await prisma.tasklist.update({
          where: {
            id: query.id?.toString(),
          },
          data: {
            id: req.body.id,
            name: req.body.name,
            status: req.body.status,
            deadStart: req.body.deadStart,
            deadEnd: req.body.deadEnd,
            deadline: req.body.deadline,
            priority: req.body.priority,
            point: req.body.point,
          },
        });
        res.status(200).send({ update });
        break;

      case "PATCH":
        const PATCH = await prisma.tasklist.update({
          where: {
            id: query.id?.toString(),
          },
          data: req.body,
        });
        res.status(200).send({ PATCH });
        break;
      default:
        res.setHeader("Allow", ["GET", "DELETE", "PUT", "PATCH"]);
        res.status(200).end(`method: ${method} not Allow`);
        break;
    }
  } catch (err) {
    res.status(500).send({ error: "failed to fetch data" });
  }
}

export default handler;
