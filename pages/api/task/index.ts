import { DeadLine, Prisma, PrismaClient, Status } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";
import { formatISO } from "date-fns";
export const prisma = new PrismaClient();

enum SortOrder {
  ASC = "asc",
  DESC = "desc",
}

interface GetTasks {
  doeDateStart: string | string[] | undefined;
  firstCellDate: string | string[] | undefined;
  doeDateend: string | string[] | undefined;
  lastCellDate: string | string[] | undefined;
  name: string | string[] | undefined;
  assigned: string | string[] | undefined;
}

async function handler(req: NextApiRequest, res: NextApiResponse) {
  let { method, query } = req;
  let sort = query.dateSort ? query.dateSort : SortOrder.ASC;
  let page = Number(query.page ? query.page : 1);
  const token = await getToken({ req, secret: process.env.JWT_SECRET });

  let statuss = query.status ? query.status.toString() : "[]";
  let deadlines = query.deadline ? query.deadline.toString() : "[]";
  let points = query.point ? query.point.toString() : "[]";
  let prioritys = query.priority ? query.priority.toString() : "[]";

  const dataPoint: number[] = [];
  for (const item of JSON.parse(points)) {
    const [start, end] = item.split("-");
    const startNum = parseInt(start);
    const endNum = parseInt(end);

    for (let i = startNum; i <= endNum; i++) {
      dataPoint.push(i);
    }
  }

  const dataPrioritys: number[] = [];
  for (const item of JSON.parse(prioritys)) {
    const [start, end] = item.split("-");
    const startNum = parseInt(start);
    const endNum = parseInt(end);

    for (let i = startNum; i <= endNum; i++) {
      dataPrioritys.push(i);
    }
  }
  let allprioritys = [0, 1, 2, 3, 4, 5];
  let allpoint = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  const getTask = (query: GetTasks) => {
    let where = {
      deadStart: {
        gte: query.doeDateStart
          ? formatISO(new Date(query.doeDateStart.toString()))
          : query.firstCellDate
          ? formatISO(new Date(query.firstCellDate.toString()))
          : undefined,
        lte: query.doeDateend
          ? formatISO(new Date(query.doeDateend.toString()))
          : query.lastCellDate
          ? formatISO(new Date(query.lastCellDate.toString()))
          : undefined,
      },
      name: query.name
        ? ({ contains: query.name, mode: "insensitive" } as Prisma.StringFilter)
        : undefined,
      point: { in: dataPoint.length == 0 ? allpoint : dataPoint },
      priority: {
        in: dataPrioritys.length == 0 ? allprioritys : dataPrioritys,
      },
      status: {
        in:
          JSON.parse(statuss).length == 0
            ? [Status.DONE, Status.PROCESSING, Status.TODO]
            : JSON.parse(statuss),
      },
      deadline: {
        in:
          JSON.parse(deadlines).length == 0
            ? [DeadLine.COMING, DeadLine.OVER, DeadLine.RUNNING]
            : JSON.parse(deadlines),
      },

      OR: [
        {
          authorID: query.assigned
            ? query.assigned == "assignes"
              ? "123"
              : token?.sub
            : token?.sub,
        },

        {
          users: {
            some: {
              user: {
                id: query.assigned
                  ? query.assigned == "mytask"
                    ? "123"
                    : token?.sub
                  : token?.sub,
              },
            },
          },
        },
      ],
    };
    return where;
  };

  try {
    switch (method) {
      case "GET":
        const datas = await prisma.tasklist.findMany({
          skip: query.page ? (page - 1) * 5 : 0,
          take: query.page ? 5 : undefined,
          include: { users: { include: { user: true } }, author: true },
          where: getTask({
            doeDateStart: query.doeDateStart,
            firstCellDate: query.firstCellDate,
            doeDateend: query.doeDateend,
            lastCellDate: query.lastCellDate,
            name: query.name,
            assigned: query.assigned,
          }),
          orderBy: [
            {
              deadStart: sort as Prisma.SortOrder,
            },
          ],
        });

        let count = await prisma.tasklist.count({
          where: getTask({
            doeDateStart: query.doeDateStart,
            firstCellDate: query.firstCellDate,
            doeDateend: query.doeDateend,
            lastCellDate: query.lastCellDate,
            name: query.name,
            assigned: query.assigned,
          }),
          orderBy: [
            {
              deadStart: sort as Prisma.SortOrder,
            },
          ],
        });
        res.status(200).send({ datas, count: count, alldata: count });
        break;

      case "POST":
        if (token?.sub) {
          const creates = await prisma.tasklist.create({
            data: {
              name: req.body.name,
              status: req.body.status,
              deadline: req.body.deadline,
              authorID: token.sub,
            },
          });
          res.status(200).send({ data: creates });
        }
        break;

      default:
        res.setHeader("Allow", ["GET", "POST"]);
        res.status(200).end(`method: ${method} not Allow`);
        break;
    }
  } catch (error) {
    res.status(500).send({ error: "failed to fetch data" });
  }
}
export default handler;
