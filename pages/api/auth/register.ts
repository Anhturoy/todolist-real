import { PrismaClient } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
var bcrypt = require("bcryptjs");

const prisma = new PrismaClient();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const { method } = req;

    switch (method) {
      case "POST":
        const checked = await prisma.user.findUnique({
          where: {
            email: req.body.email,
          },
        });
        if (checked) {
          res.status(200).send({ message: "Account already exists" });
        }
        let passwordBcyt = await bcrypt.hash(req.body.password, 10);
        if (passwordBcyt) {
          const newUser = await prisma.user.create({
            data: {
              email: req.body.email,
              password: passwordBcyt,
              username: req.body.username,
              background: req.body.background
            },
          });
          res.status(200).send({ data: newUser });
        }
        break;

      default:
        res.setHeader("Allow", ["POST"]);
        res.status(200).end(`method: ${method} not Allow`);
        break;
    }
  } catch (err) {
    res.status(500).send({ error: "failed to fetch data" });
  }
}
