import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { PrismaClient } from "@prisma/client";

var bcrypt = require("bcryptjs");

export const prisma = new PrismaClient();

export const authOptions = {
  session: {
    jwt: true, // sử dụng JWT để mã hóa session
    maxAge: 60 * 15, // thời gian sống tối đa của session (15 phút)
    // maxAge: 30 * 24 * 60 * 60, // thời gian sống tối đa của session (30 ngày)
  },
  // Configure one or more authentication providers
  providers: [
    CredentialsProvider({
      type: "credentials",
      credentials: {
        email: { label: "Email", type: "text" },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, req) {
        const { email, password } = credentials as {
          email: string;
          password: string;
        };
        const userdata = await prisma.user.findUnique({
          where: {
            email: email,
          },
        });
        const checked = await bcrypt.compare(password, userdata?.password);
        if (!userdata) {
          throw new Error("wrong email");
        }
        if (!checked) {
          throw new Error("wrong password");
        }
        if (userdata && checked) {
          return {
            id: userdata.id,
            email: JSON.stringify(userdata),
            name: userdata.id,
          };
        } else {
          return null;
        }
      },
    }),
  ],
  pages: {
    signIn: "/auth/login",
    signOut: "/auth/login",
  },
  jwt: {
    secret: process.env.JWT_SECRET,
  },
  database: process.env.DATABASE_URL,
};

export default NextAuth(authOptions);
