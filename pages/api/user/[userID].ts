import { PrismaClient } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";

export const prisma = new PrismaClient();

async function handler(req: NextApiRequest, res: NextApiResponse) {
  let { method , query  } = req;
  const token = await getToken({ req, secret: process.env.JWT_SECRET });
  try {
    switch (method) {
      case "GET":
        const data = await prisma.user.findMany({
          where: { id: token?.sub },
          include: {
            tasks: {include: {task:true}},
            images: true
          }
        });
        res.status(200).send({ data: data });
        break;

        case "PATCH":
          const patchdata = await prisma.user.updateMany({
            where: { id:token?.sub },
            data : {background: req.body.background}
          });
          res.status(200).send({ data: patchdata });
          break;

      default:
        res.setHeader("Allow", ["GET","PATCH"]);
        res.status(200).end(`method: ${method} not Allow`);
        break;
    }
  } catch (error) {
    res.status(500).send({ error: "failed to fetch data" });
  }
}
export default handler;
