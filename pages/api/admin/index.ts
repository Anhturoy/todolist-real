import { PrismaClient } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";

export const prisma = new PrismaClient();

async function handler(req: NextApiRequest, res: NextApiResponse) {
  let { method } = req;
  const token = await getToken({ req, secret: process.env.JWT_SECRET });

  try {
    switch (method) {
      case "GET":
        const data = await prisma.user.findMany();
        res.status(200).send({ data, token });
        break;

      default:
        res.setHeader("Allow", ["GET"]);
        res.status(200).end(`method: ${method} not Allow`);
        break;
    }
  } catch (error) {
    res.status(500).send({ error: "failed to fetch data" });
  }
}
export default handler;
