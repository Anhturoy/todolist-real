import { PrismaClient } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";

export const prisma = new PrismaClient();

async function handler(req: NextApiRequest, res: NextApiResponse) {
  let { method } = req;

  const token = await getToken({ req, secret: process.env.JWT_SECRET });
  try {
    switch (method) {
      case "POST":
        if (token?.sub) {
          let A = await prisma.user.findUnique({
            where: { email: req.body.email },
          });
          if (A) {
            if (token.sub == A.id) {
              res.status(200).send({ message: "You là chủ task mà bro ??" });
            } else {
              const checked1 = await prisma.usersOnTask.findMany({
                where: {
                  userID: A.id ? A.id.toString() : "",
                  taskID: req.body.taskID ? req.body.taskID?.toString() : "",
                },
              });
              const checked2 = await prisma.user.findUnique({
                where: { id: A.id },
              });
              if (checked1.length > 0) {
                res.status(200).send({ message: "đã thêm trước đó" });
              } else {
                if (!checked2) {
                  res.status(200).send({ message: "iD khong chinh xac!!" });
                }
                const data = await prisma.usersOnTask.create({
                  data: {
                    userID: A.id ? A.id.toString() : "",
                    taskID: req.body.taskID ? req.body.taskID?.toString() : "",
                    assignedBy: token.sub,
                    message: req.body.message,
                  },
                });
                res
                  .status(200)
                  .send({ data: data, message: "Đã thêm thành công" });
              }
            }
          } else {
            res.status(200).send({ message: "Email khong chinh xac!!" });
          }
        }
        break;

      case "PATCH":
        if (token?.sub) {
          const deleted = await prisma.usersOnTask.delete({
            where: {
              userID_taskID: {
                userID: req.body.userID,
                taskID: req.body.taskID,
              },
            },
          });
          res.status(200).send({ data: deleted });
        }
        break;

      default:
        res.setHeader("Allow", ["GET", "PATCH"]);
        res.status(200).end(`method: ${method} not Allow`);
        break;
    }
  } catch (error) {
    res.status(500).send({ error: "failed to fetch data" });
  }
}
export default handler;
