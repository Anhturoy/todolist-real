import { PrismaClient } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";

export const prisma = new PrismaClient();

async function handler(req: NextApiRequest, res: NextApiResponse) {
  let { method } = req;

  const token = await getToken({ req, secret: process.env.JWT_SECRET });
  try {
    switch (method) {
      case "POST":
        if(token?.sub){
          const checked = await prisma.image.findMany({
            where:{
              userId: token.sub,
              filename:  req.body.data 
            }
          })
          if(checked.length > 0){
            const patchdata = await prisma.user.updateMany({
              where: { id:token?.sub },
              data : {background:  checked[0].filename}
            });
            res.status(200).send({ data: checked[0], token });
          }else{
            const savedImage = await prisma.image.create({
              data: { filename: req.body.data , userId: token.sub.toString() },
            });         
          res.status(200).send({ data: savedImage, token });
          }
        }
        break;

      default:
        res.setHeader("Allow", ["POST"]);
        res.status(200).end(`method: ${method} not Allow`);
        break;
    }
  } catch (error) {
    res.status(500).send({ error: "failed to fetch data" });
  }
}
export default handler;
