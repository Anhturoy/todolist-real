import { PrismaClient } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";
import { getToken } from "next-auth/jwt";

export const prisma = new PrismaClient();

async function handler(req: NextApiRequest, res: NextApiResponse) {
  let { method, query } = req;

  const token = await getToken({ req, secret: process.env.JWT_SECRET });
  try {
    switch (method) {
      case "DELETE":
        if(token?.sub){
            const data = await prisma.image.delete({
                where:{
                    id: query.imageID?.toString()
                }
            })
            res.status(200).send({ data: data });
        }
        break;

      default:
        res.setHeader("Allow", ["DELETE"]);
        res.status(200).end(`method: ${method} not Allow`);
        break;
    }
  } catch (error) {
    res.status(500).send({ error: "failed to fetch data" });
  }
}
export default handler;
