import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { SessionProvider } from "next-auth/react";
import { Provider } from "react-redux";
import { store } from "../redux/store";
import Auth from "./auth";

// Modify the CustomAppProps type to include the correct Component type
type CustomAppProps = AppProps & {
  Component: React.ComponentType & { auth?: boolean }; // Change NextComponentType to React.ComponentType
};

export default function App({
  Component,
  pageProps: { session, ...pageProps },
}: any) {
  return (
    <SessionProvider session={session}>
      {Component.auth ? (
        <Auth>
          <Provider store={store}>
            <Component {...pageProps} />
          </Provider>
        </Auth>
      ) : (
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      )}
    </SessionProvider>
  );
}
