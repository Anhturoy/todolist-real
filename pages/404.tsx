import React from 'react';

const NotFoundPage = () => {
  return (
    <div className='w-screen h-screen flex items-center flex-col justify-center'>
      <h1>
        <img  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQjhTEjbjeF0qA5P7KAheKK6CKhkmx6fNczQlAkIVHpK2DUKGOzcw9A291-4k54GaMNRU&usqp=CAU" alt="" />
      </h1>
      <p>The requested page could not be found.</p>
    </div>
  );
};

export default NotFoundPage;