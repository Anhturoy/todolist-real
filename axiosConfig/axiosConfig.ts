import axios from 'axios';

const axiosConfig = axios.create({
  baseURL: '/', // Thay thế với URL cơ sở của API của bạn
//   timeout: 5000, // Đặt giá trị thời gian chờ (timeout) theo milliseconds
//   // Thêm bất kỳ tùy chọn cấu hình Axios nào bạn cần
});

export default axiosConfig;