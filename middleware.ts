import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";
import { getToken } from "next-auth/jwt";

export async function middleware(req: NextRequest) {
  const token = await getToken({ req, secret: process.env.JWT_SECRET });
  if (!token) {
    return NextResponse.redirect(new URL("/auth/login", req.url));
  } else {
    if (req.nextUrl.pathname.includes("/containers")  ) {
      return NextResponse.redirect(new URL("/homepage", req.url));
    }
    if (token.email !== undefined) {
      let role = JSON.parse(token.email ? token.email : "").role;
      if (role == "ADMIN") {
        if (req.nextUrl.pathname.includes("/api/admin")) {
          return NextResponse.next();
        }
        if (req.nextUrl.pathname.includes("/homepage") || req.nextUrl.pathname.includes("/calender") || req.nextUrl.pathname.includes("/detailTask")) {
          return NextResponse.redirect(new URL("/admin/DashBoard", req.url));
        }
      } else {
        if (req.nextUrl.pathname.includes("/admin")) {
          return NextResponse.redirect(new URL("/", req.url));
        }
        if (req.nextUrl.pathname.includes("/api/user")) {
          return NextResponse.redirect(new URL("/", req.url));
        }
      }
    }
    return NextResponse.next();
  }
}

export const config = {
  matcher: [
    "/homepage/:homeID*",
    "/detaiTask/:detailID*",
    "/containers/:path*",
    "/calender",
    "/api/task/:id*",
    "/api/task",
    "/api/admin",
    "/admin/DashBoard",
    "/detailTask/:detailID*"
  ],
};
