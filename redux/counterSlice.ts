import { createSlice } from '@reduxjs/toolkit';

const initialState  ={
  loading: false,
  deleLoading: false,
  createLoading: false,
  uploadImgLoading: false,
  editToggle: false,
  dataUsers: null,
  AlldataTasks: null,
  dataTasks: [],
  dataTasksBackUp: [],
  imageUrl: null,
  imageList: [],
  dataCalender: null
}
const counterSlice = createSlice({
  name: 'counter',
  initialState: initialState,
  reducers: {
   setloading: (state, action) =>{
    state.loading = action.payload
   },
   setDeleLoading: (state, action)=>{
     state.deleLoading = action.payload
   },
   setUsers: (state, action) =>{
    state.dataUsers = action.payload
   },
   setTasks: (state, action) =>{
    state.dataTasks = action.payload
   },
   setTasksBackUp: (state, action)=>{
    state.dataTasksBackUp = action.payload
   },
   setImageUrl: (state, action)=>{
    state.imageUrl = action.payload
   },
   setImageList: (state, action) =>{
    state.imageList = action.payload
   },
   setCreateLoading : (state, action)=>{
    state.createLoading = action.payload
   },
   setUploadImgLoading : (state, action)=>{
    state.uploadImgLoading = action.payload
   },
   setAlldataTasks :(state,action)=>{
    state.AlldataTasks = action.payload
   },
   setdataCalender: (state,action) =>{
    state.dataCalender = action.payload
   },
   setEditToggle: (state,action) =>{
    state.editToggle = action.payload
   }
  },
});

export const {setloading , setUsers ,setEditToggle, setTasks, setdataCalender, setTasksBackUp, setDeleLoading, setImageUrl, setImageList,setAlldataTasks, setCreateLoading, setUploadImgLoading} = counterSlice.actions;
export default counterSlice.reducer;