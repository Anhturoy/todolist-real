import { DeadLine, Status } from "@prisma/client";

export const filterOptionDeadline = [
  {
    lable: "Hết hạn",
    key: DeadLine.OVER,
  },
  {
    lable: "Đang diễn ra",
    key: DeadLine.RUNNING,
  },
  {
    lable: "Sắp tới",
    key: DeadLine.COMING,
  },
];

export const filterOptionSort = [
  {
    lable: "Cũ nhất",
    key: "asc",
  },
  {
    lable: "Mới nhất",
    key: "desc",
  },
];

export const filterOptionPriority = [
  {
    lable: "set level ",
    key: "0-0",
  },
  {
    lable: "level 1",
    key: "1-1",
  },
  {
    lable: "level 2-3",
    key: "2-3",
  },
  {
    lable: "level 4-5",
    key: "4-5",
  },
];

export const filterOptionPoint = [
  {
    lable: "set point",
    key: "0-0",
  },
  {
    lable: "point 1-4",
    key: "1-4",
  },
  {
    lable: "point 5-7",
    key: "5-7",
  },
  {
    lable: "point 8-10",
    key: "8-10",
  },
];

export const filterOptionStatus = [
  {
    lable: "Getall",
    key: "getall",
  },
  {
    lable: "Todo",
    key: Status.TODO,
  },
  {
    lable: "Processing",
    key: Status.PROCESSING,
  },
  {
    lable: "Done",
    key: Status.DONE,
  },
];

export const filterOptionStatus2 = [
  {
    lable: "Todo",
    key: Status.TODO,
  },
  {
    lable: "Processing",
    key: Status.PROCESSING,
  },
  {
    lable: "Done",
    key: Status.DONE,
  },
];


export const filterOptionAssigned = [
  {
    lable: "My tasks",
    key: "mytask",
  },
  {
    lable: "Assignes",
    key: "assignes",
  },
];

export const SelectOption = [
  {
    defaultValue: "Status",
    option: [
      { value: "TODO", label: "TODO" },
      { value: "PROCESSING", label: "PROCESSING" },
      { value: "DONE", label: "DONE" },
    ],
  },
  {
    defaultValue: "Deadline",
    option: [
      { value: "OVER", label: "OVER" },
      { value: "DOING", label: "DOING" },
      { value: "COMING", label: "COMING" },
    ],
  },
  {
    defaultValue: "Point",
    option: [
      { value: "0-0", label: "set point" },
      { value: "1-4", label: "1-4" },
      { value: "5-7", label: "5-7" },
      { value: "8-10", label: "8-10" },
    ],
  },
  {
    defaultValue: "Priority",
    option: [
      {label: "set level",value: "0-0"},
      {label: "level 1",value: "1-1"},
      {label: "level 2-3",value: "2-3"},
      {label: "level 4-5",value: "4-5"},
    ],
  },
  {
    defaultValue: "Assinges",
    option: [
      {label: "My tasks",value: "mytask"},
      {label: "Assignes",value: "assignes"},
    ],
  },
];
